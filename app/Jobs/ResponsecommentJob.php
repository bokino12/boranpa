<?php

namespace App\Jobs;

use App\Notifications\ResponsecommentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ResponsecommentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fromBodyUser;
    protected $uploadvideo;
    protected $video;
    protected $comment;
    protected $userFrom;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fromBodyUser,$uploadvideo,$video,$comment,$userFrom)
    {
        $this->fromBodyUser = $fromBodyUser;
        $this->uploadvideo = $uploadvideo;
        $this->video = $video;
        $this->comment = $comment;
        $this->userFrom = $userFrom;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment->user
            ->notify(new ResponsecommentNotification(
                $this->fromBodyUser,
                $this->uploadvideo,
                $this->video,
                $this->comment,
                $this->userFrom));

    }
}
