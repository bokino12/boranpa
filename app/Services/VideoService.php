<?php


namespace App\Services;


use App\Http\Resources\UploadvideoResource;
use App\Http\Resources\VideoResource;
use App\Models\uploadvideo;
use App\Models\video;
use App\Models\videotype;
use Illuminate\Support\Facades\Auth;

class VideoService
{

    public static function getuploadslidepage($q, $videotype)
    {
        return $q->with('videotype', 'uploadimagealable')
            ->whereIn('uploadimagealable_id', [$videotype->id])
            ->where(['status' => 1])
            ->where('uploadimagealable_type', videotype::class);
    }


    public static function apivideotypeslide($videotype)
    {
        $data = videotype::whereSlug($videotype->slug)
            ->where(['status' => 1])
            ->withCount(['uploadimages' => function ($q) use ($videotype) {
                self::getuploadslidepage($q, $videotype);
            }])
            ->with(['uploadimages' => function ($q) use ($videotype) {
                self::getuploadslidepage($q, $videotype)
                    ->orderByDesc('created_at')->distinct()->get();
            }])
            ->firstOrFail();

        return $data;
    }


    public static function apivideobyvideotype($videotype)
    {
        $videos = VideoResource::collection($videotype->videos()
            ->with('user', 'videotype')
            ->whereIn('videotype_id', [$videotype->id])
            ->withCount(['uploadacteurs' => function ($q) {
                $q->where(['status' => 1]);
            }])
            ->withCount(['uploadvideos' => function ($q) {
                $q->where(['status' => 1]);
            }])
            ->whereHas('videotype', function ($q) {
                $q->where('status', 1);
            })
            ->where(['status' => 1])->latest()->get());

        return $videos;
    }


    public static function apivideoshow($video)
    {
        $video = video::whereSlug($video->slug)
            ->with('user', 'videotype', 'uploadacteurs')
            ->with(['uploadacteurs' => function ($q) use ($video) {
                $q->with('acteur')
                    ->whereIn('uploadacteuralable_id', [$video->id])
                    ->where('uploadacteuralable_type', video::class)
                    ->get();
            }])
            ->withCount(['uploadacteurs' => function ($q) {
                $q->where(['status' => 1]);
            }])
            ->withCount(['uploadvideos' => function ($q) {
                $q->where(['status' => 1]);
            }])
            ->whereHas('videotype', function ($q) {
                $q->where('status', 1);
            })
            ->where(['status' => 1])
            ->firstOrFail();

        return $video;
    }

    public static function apivideositeshow($video)
    {
        $video = UploadvideoResource::collection(
            $video->uploadvideos()
                ->with(['uploadvideoalable' => function ($q) {
                    $q->where(['status' => 1])
                        ->select('slug','id')->get();
                }])
                ->where(['status' => 1])
                ->whereIn('uploadvideoalable_id', [$video->id])
                ->where('uploadvideoalable_type', video::class)
                ->get());

        return $video;
    }

    public static function apivideosviewshow($uploadvideo,$video_id)
    {
        $video = UploadvideoResource::collection(uploadvideo::where(['status' => 1])
            ->with(['uploadvideoalable' => function ($q) {
                $q->where(['status' => 1])
                    ->select('id','status','slug','title')->get();
            }])
            ->whereIn('uploadvideoalable_id', [$video_id])
            ->where('uploadvideoalable_type', video::class)
            ->orderByDesc('episode_video')->get());

        return $video;
    }

    public static function apiviewshow($uploadvideo,$video)
    {
        $video = uploadvideo::whereSlug($uploadvideo->slug)
            ->with('uploadvideoalable')
            ->where(['status' => 1])
            ->firstOrFail();

        return $video;
    }

}
