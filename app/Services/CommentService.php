<?php


namespace App\Services;


use App\Http\Resources\UploadvideoResource;
use App\Http\Resources\VideoResource;
use App\Jobs\ResponsecommentJob;
use App\Models\uploadvideo;
use App\Models\video;
use App\Models\videotype;
use Illuminate\Support\Facades\Auth;

class CommentService
{

    public static function newEmailToresponsecommentpageShow($request,$uploadvideo,$video,$comment)
    {
        $userFrom = auth()->user();
        $fromBodyUser = $request->get('body');

        // Ici je verify si status_responsecomments === 1 est active
        if ($comment->user->profile->status_comments){

            $emailToUser = (new ResponsecommentJob($fromBodyUser,$uploadvideo,$video,$comment,$userFrom));
            dispatch($emailToUser);

        }
    }

}
