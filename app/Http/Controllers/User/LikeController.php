<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\StoreRequest;
use App\Models\comment;
use App\Models\contact;
use App\Models\favorite;
use App\Models\responsecomment;
use App\Models\uploadvideo;
use App\Models\user;
use App\Models\video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param video $video
     * @return \Illuminate\Http\JsonResponse
     */
    public function likevideo(Request $request,video $video)
    {
        $response = $video->likes()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unlikevideo(Request $request,video $video)
    {
        $response = auth()->user()->removelikes()->toggle($video);

        return response()->json(['success'=>$response]);
    }

    /**
     * @param Request $request
     * @param uploadvideo $uploadvideo
     * @return \Illuminate\Http\JsonResponse
     */
    public function likeupvideo(Request $request,uploadvideo $uploadvideo)
    {
        $response = $uploadvideo->likes()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unlikeupvideo(Request $request,uploadvideo $uploadvideo)
    {
        $response = auth()->user()->removelikes()->toggle($uploadvideo);

        return response()->json(['success'=>$response]);
    }

    /**
     * @param Request $request
     * @param comment $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function likecomment(Request $request,comment $comment)
    {
        $response = $comment->likes()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unlikecomment(Request $request,comment $comment)
    {
        $response = auth()->user()->removelikes()->toggle($comment);

        return response()->json(['success'=>$response]);
    }

    /**
     * @param Request $request
     * @param responsecomment $responsecomment
     * @return \Illuminate\Http\JsonResponse
     */
    public function likerspcomment(Request $request,responsecomment $responsecomment)
    {
        $response = $responsecomment->likes()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unlikerspcomment(Request $request,responsecomment $responsecomment)
    {
        $response = auth()->user()->removelikes()->toggle($responsecomment);

        return response()->json(['success'=>$response]);
    }

}
