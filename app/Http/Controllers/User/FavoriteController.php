<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\StoreRequest;
use App\Models\contact;
use App\Models\favorite;
use App\Models\uploadvideo;
use App\Models\user;
use App\Models\video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function apifavorites()
    {
        $user = Auth::user();

        $this->authorize('update',$user);

        $favorites =  user::whereSlug($user->slug)
            ->withCount(['favoritesvideos' => function ($q) use ($user) {
                $q->whereIn('user_id', [$user->id])
                    ->with('user', 'favoriteable')
                    ->where('favoriteable_type', video::class)
                    ->whereIn('user_id', [$user->id])
                    ->with(['favoriteable.videotype' => function ($q) {
                        $q->where('status', 1);},]);}])

            ->with(['favoritesvideos' => function ($q) use ($user){
                $q->with('favoriteable','user')
                    ->where('favoriteable_type',video::class)
                    ->whereIn('user_id', [$user->id])
                    ->with(['favoriteable.videotype' => function ($q){
                        $q->where('status',1)->distinct()->get();},
                        'favoriteable.user' => function ($q){
                            $q->distinct()->select('id','username','slug');}])
                    ->orderBy('created_at','DESC')->get()->toArray()
                ;},
            ])
            ->withCount(['favoritesuploadvideos' => function ($q) use ($user) {
                $q->whereIn('user_id', [$user->id])
                    ->with('user', 'favoriteable')
                    ->where('favoriteable_type', uploadvideo::class)
                    ->whereIn('user_id', [$user->id]);}])

            ->with(['favoritesuploadvideos' => function ($q) use ($user){
                $q->with('favoriteable','user')
                    ->where('favoriteable_type',uploadvideo::class)
                    ->whereIn('user_id', [$user->id])
                    ->with(['user' => function ($q){
                        $q->select(['id','username','slug'])->get();}])
                    ->with(['favoriteable.uploadvideoalable' => function ($q){
                        $q->where('status',1)->select(['id','slug','status'])->get();}])
                    ->orderBy('created_at','DESC')->get()->toArray()
                ;},
            ])->first();


        return response()->json($favorites, 200);
    }

    public function index()
    {
        return view('site.video.favorite');
    }

    public function videos()
    {
        return view('site.video.favorite');
    }

    /**
     * @param Request $request
     * @param video $video
     * @return \Illuminate\Http\JsonResponse
     */
    public function favoritevideo(Request $request,video $video)
    {
        $response = $video->favorites()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unfavoritevideo(Request $request,video $video)
    {
        $response = auth()->user()->removefavorites()->detach($video);

        return response()->json(['success'=>$response]);
    }

    /**
     * @param Request $request
     * @param uploadvideo $uploadvideo
     * @return \Illuminate\Http\JsonResponse
     */
    public function favoriteupvideo(Request $request,uploadvideo $uploadvideo)
    {
        $response = $uploadvideo->favorites()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unfavoriteupvideo(Request $request,uploadvideo $uploadvideo)
    {
        $response = auth()->user()->removefavorites()->detach($uploadvideo);

        return response()->json(['success'=>$response]);
    }

    public function userunfavoritevideo(favorite $favorite)
    {
        $user = Auth::user();

        $this->authorize('update',$user);

        $favorite->delete();
    }

}
