<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\uploadvideo;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{

    public function index()
    {
        return view('site.pages.search');
    }

    public function search_unit_by_key()
    {
        $key = Request::get('q');
        $uploadvideo = uploadvideo::where('title','LIKE',"%{$key}%")
            ->orWhere('is_active','LIKE',"%{$key}%")
            ->get();

        return response()->json([ 'uploadvideo' => $uploadvideo ],Response::HTTP_OK);
    }


}
