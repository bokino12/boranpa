<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UploadimageResource;
use App\Models\boutique;
use App\Models\uploadimage;
use Illuminate\Http\Request;

class BoutiqueController extends Controller
{


    public function apislideboutique()
    {

        $uploadimages = UploadimageResource::collection(uploadimage::where(
            ['status' => 1,'uploadimagealable_type' => boutique::class]
        )->orderByDesc('created_at')->distinct()->get());

        return response($uploadimages,200);
    }

    public function boutique()
    {
        return view('site.pages.boutique');
    }


}
