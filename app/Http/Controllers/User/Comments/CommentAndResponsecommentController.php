<?php

namespace App\Http\Controllers\User\Comments;

use App\Http\Controllers\Controller;
use App\Models\comment;
use App\Models\responsecomment;
use Illuminate\Http\Request;

class CommentAndResponsecommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function updateresponse(Request $request,responsecomment $responsecomment)
    {
        $validatedData = $request->validate(['body' => 'required|min:2|max:5000']);

        $responsecomment->update([
            'body' => $validatedData['body'],
        ]);

        return $responsecomment->toJson();
    }

    public function sendresponse(Request $request,responsecomment $responsecomment)
    {
        $validatedData = $request->validate(['body' => 'required|min:2|max:5000']);

        $responsecomment = responsecomment::create([
            'body' => $validatedData['body'],
            'comment_id' => $responsecomment->comment_id,
            'to_id' => $responsecomment->user_id,
        ]);

        return $responsecomment->toJson();
    }

    public function destroy(comment $comment)
    {
        $comment->delete();

        return ['message' => 'Deleted successfully '];
    }

    public function destroyresponse(responsecomment $responsecomment)
    {
        $responsecomment->delete();

        return ['message' => 'Deleted successfully '];
    }

}
