<?php

namespace App\Http\Controllers\User\Comments;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Models\comment;
use App\Models\responsecomment;
use App\Models\uploadvideo;
use App\Services\CommentService;
use Illuminate\Http\Request;

class CommentuploadvideoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => [
            'getcomment',
        ]]);
    }


    public function getcomment(uploadvideo $uploadvideo,$video)
    {
        $comments = CommentResource::collection($uploadvideo->comments()
            ->with('user','commentable','responsecomments')
            ->whereIn('commentable_id',[$uploadvideo->id])
            ->where('status',1)
            ->where('commentable_type',uploadvideo::class)
            ->with(['responsecomments' => function ($q){
                $q->with('user','to','comment')
                    ->orderBy('created_at','asc')
                    ->responseactive()->distinct()->get();},
            ])->orderByDesc('created_at')->distinct()->get());

        return response()->json($comments,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */

    public function sendcomment(Request $request,uploadvideo $uploadvideo,$video)
    {
        $this->validate($request,[
            'body'=>'required|max:5000',
        ]);

        $comment = $uploadvideo->comments()->create($request->all());

        return response()->json($comment,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatecomment(Request $request,uploadvideo $uploadvideo,$video,comment $comment)
    {

        $validatedData = $request->validate(['body' => 'required|max:5000']);

        $comment->update([ 'body' => $validatedData['body'],]);

        return response()->json($comment,200);
    }


    public function sendresponsecomment(Request $request,uploadvideo $uploadvideo,$video,comment $comment)
    {
        $validatedData = $request->validate(['body' => 'required|min:2|max:5000']);

        $responsecomment = responsecomment::create([
            'body' => $validatedData['body'],
            'comment_id' => $comment->id,
            'to_id' => $comment->user_id,
        ]);

        CommentService::newEmailToresponsecommentpageShow($request,$uploadvideo,$video,$comment);

        return $responsecomment->toJson();
    }

}
