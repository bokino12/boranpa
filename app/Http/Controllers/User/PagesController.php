<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\StoreRequest;
use App\Models\contact;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function about()
    {
        return view('site.pages.about');
    }

    public function faq()
    {
        return view('site.pages.faq');
    }

    public function detail()
    {
        return view('site.pages.detail');
    }


}
