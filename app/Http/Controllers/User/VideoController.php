<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UploadvideoResource;
use App\Http\Resources\VideoResource;
use App\Http\Resources\VideotypeResource;
use App\Models\uploadimage;
use App\Models\uploadvideo;
use App\Models\video;
use App\Models\videotype;
use App\Services\VideoService;
use Illuminate\Http\Request;

class VideoController extends Controller
{


    public function apivideotypes()
    {
        $videotypes = videotype::where(['status' => 1])
            ->orderByDesc('created_at')->distinct()->get();

        return response($videotypes,200);
    }

    public  function apivideotypeslide(videotype $videotype)
    {
        $videotype = new VideotypeResource(VideoService::apivideotypeslide($videotype));

        return response()->json($videotype,200);
    }

    public function apivideobyvideotype(videotype $videotype)
    {
        $videos = VideoService::apivideobyvideotype($videotype);

        return response()->json($videos,200);
    }

    public function apivideoshow(video $video)
    {
        $data = new VideoResource(VideoService::apivideoshow($video));

        return response()->json($data,200);
    }

    public function apivideositeshow(video $video)
    {

        $data = VideoService::apivideositeshow($video);

        return response()->json($data,200);
    }

    public function apivideosviewshow(uploadvideo $uploadvideo,$video_id)
    {
        $data = VideoService::apivideosviewshow($uploadvideo ,$video_id);

        return response()->json($data,200);
    }

    public function apiviewshow(uploadvideo $uploadvideo,$video)
    {
        $expiresAt = now()->addSeconds(5);
        views($uploadvideo)
            ->cooldown($expiresAt)
            ->record();

        $data = new UploadvideoResource(VideoService::apiviewshow($uploadvideo,$video));

        return response()->json($data,200);
    }

    public function videobyvideotype(videotype $videotype)
    {
        return view('site.video.category',compact('videotype'));
    }

    public function videoshow(video $video)
    {
        return view('site.video.show',compact('video'));
    }

    public function viewshow(uploadvideo $uploadvideo,$video)
    {
        return view('site.video.view',compact('uploadvideo'));
    }
}
