<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\user;
use App\Models\video;
use Illuminate\Http\Request;

class ActeurController extends Controller
{

    public function apiactors(user $user)
    {

        $user = user::whereSlug($user->slug)
            ->withCount(['videosactors' => function ($q) use ($user) {
                $q->with('acteur', 'uploadacteuralable')
                    ->where('uploadacteuralable_type', video::class)
                    ->whereHasMorph('uploadacteuralable', video::class , function($q){
                        $q->where(['status' => 1]);})
                    ->whereIn('acteur_id', [$user->id]);
            }])
            ->with(['videosactors' => function ($q) use ($user) {
                $q->with('acteur', 'uploadacteuralable')
                    ->where('uploadacteuralable_type', video::class)
                    ->whereIn('acteur_id', [$user->id])
                    ->with(['uploadacteuralable.videotype' => function ($q) {
                            $q->select('id', 'name', 'slug');},])
                    ->whereHasMorph('uploadacteuralable', video::class , function($q){
                        $q->where(['status' => 1]);
                    })
                    ->latest()->distinct()->get()->toArray();
            },
            ])
            ->first();



        return response($user,200);
    }

    public function index(user $user)
    {
        return view('site.video.user',compact('user'));
    }
}
