<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\todo;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function api()
    {
        
        $todos = todo::latest()->get();

        return response()->json($todos, 200);
    }
    
    public function new()
    {
        return view('home');
    }

    
    public function store(Request $request)
    {
        $validatedData = $request->validate(['name' => 'required|min:2|max:5000']);

        $responsedata = todo::create([
            'name' => $validatedData['name'],
        ]);

        return $responsedata->toJson();
    }
}
