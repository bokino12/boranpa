<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UploadimageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $this->status,
            'photo' => $this->photo,
            'slug' => $this->slug,
            'slugin' => $this->slugin,
            'link_image' => $this->link_image,
            'color_button' => $this->color_button,
            'description' => $this->description,
            'videotype_id' => $this->videotype_id,
            'videotype' => $this->videotype,
            'uploadimagealable_type' => $this->uploadimagealable_type,
            'uploadimagealable_id' => $this->uploadimagealable_id,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
