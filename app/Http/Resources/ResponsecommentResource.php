<?php

namespace App\Http\Resources;

use App\Models\responsecomment;
use Illuminate\Http\Resources\Json\JsonResource;

class ResponsecommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'likeked' => $this->likeked(),
            'likeked_count' => $this->likes()
                ->whereIn('likeable_id',[$this->id])
                ->where('likeable_type', responsecomment::class)
                ->count(),
            'status' => $this->status,
            'user_id' => $this->user_id,
            'user' => $this->user,
            'to_id' => $this->to_id,
            'to' => $this->to,
            'comment_id' => $this->comment_id,
            'body' => $this->body,
            'ip' => $this->ip,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
