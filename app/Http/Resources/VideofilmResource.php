<?php

namespace App\Http\Resources;

use App\Models\video;
use Illuminate\Http\Resources\Json\JsonResource;

class VideofilmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $this->status,
            'photo' => $this->photo,
            'photo_cover' => $this->photo_cover,
            'slugin' => $this->slugin,
            'years_production' => $this->years_production,
            'link_video' => $this->link_video,
            'link_bande_annonce_video' => $this->link_bande_annonce_video,
            'ip' => $this->ip,
            'videotype' => $this->categoryvideo,
            'categoryvideo_id' => $this->categoryvideo_id,
            'description' => $this->description,
            'slug' => $this->slug,
            'user' => $this->user,
            'user_id' => $this->user_id,
            'likeked' => $this->likeked(),
            'countlikes' => $this->likes()
                ->whereIn('likeable_id',[$this->id])
                ->where('likeable_type', video::class)
                ->count(),
            'uploadacteurs' => $this->uploadacteurs()
                ->with('acteur')
                ->whereIn('uploadacteuralable_id',[$this->id])
                ->where('uploadacteuralable_type', video::class)
                ->get(),
            'uploadacteurs_count' => $this->uploadacteurs()
                ->with('acteur')
                ->whereIn('uploadacteuralable_id',[$this->id])
                ->where('uploadacteuralable_type', video::class)
                ->count(),
            'uploadvideos' => $this->uploadvideos()
                ->with('acteur')
                ->whereIn('uploadvideoalable_id',[$this->id])
                ->where('uploadvideoalable_type', video::class)
                ->get(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
