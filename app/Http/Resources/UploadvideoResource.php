<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UploadvideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slugin' => $this->slugin,
            'description' => $this->description,
            'years_production' => $this->years_production,
            'saison_number' => $this->saison_number,
            'photo' => $this->photo,
            'slug' => $this->slug,
            'link_bande_annonce_video' => $this->link_bande_annonce_video,
            'link_video' => $this->link_video,
            'title' => $this->title,
            'uploadvideoalable' => $this->uploadvideoalable,
            'videotime_red' => $this->videotime_red,
            'status' => $this->status,
            'views_count' => $this->viewscount()->count(),
            'favorited' => $this->favorited(),
            'likeked' => $this->likeked(),
            'episode_video' => $this->episode_video,
            'uploadvideoalable_type' => $this->uploadvideoalable_type,
            'uploadvideoalable_id' => $this->uploadvideoalable_id,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
