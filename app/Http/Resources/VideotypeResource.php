<?php

namespace App\Http\Resources;

use App\Models\videotype;
use Illuminate\Http\Resources\Json\JsonResource;

class VideotypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'label' => $this->label,
            'status' => $this->status,
            'uploadimages_count' => $this->uploadimages_count,
            'uploadimages' => $this->uploadimages()
                ->whereIn('uploadimagealable_id',[$this->id])
                ->where('uploadimagealable_type', videotype::class)
                ->get(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
