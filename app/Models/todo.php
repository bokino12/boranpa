<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class todo extends Model
{
    protected $guarded = [];

    protected $table = 'todos';

    protected $casts = [
        'status' => 'boolean',
    ];
}
