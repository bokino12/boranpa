<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contactservice extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'contactservices';

    public function from()
    {
        return $this->belongsTo(user::class,'from_id');
    }

    public function to()
    {
        return $this->belongsTo(user::class,'to_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            $model->ip = request()->ip();
        });
        static::updating(function($model){
            $model->ip = request()->ip();
        });
    }

    protected $casts = [
        'status_red' => 'boolean',
        'status_archvement' => 'boolean',
        'status_favorite' => 'boolean'
    ];

    public function contactserviceable()
    {
        return $this->morphTo();
    }
}
