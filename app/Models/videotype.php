<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class videotype extends Model
{

    protected $guarded = [];

    protected $table = 'videotypes';

    protected $casts = [
        'status' => 'boolean',
    ];

    public function uploadimages()
    {
        return $this->morphMany(uploadimage::class ,'uploadimagealable')
            ->orderByDesc('created_at');
    }

    public function videos()
    {
        return $this->hasMany(video::class, 'videotype_id');
    }
}
