<?php

namespace App\Models;

use CyrildeWit\EloquentViewable\Contracts\Viewable;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class uploadvideo extends Model implements Viewable
{
    use InteractsWithViews;

    protected $fillable = [
        'link_video',
        'slugin',
        'title',
        'videotime_red',
        'status',
        'description',
        'years_production',
        'episode_videos'
    ];

    protected $table = 'uploadvideos';

    protected $casts = [
        'status' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            $model->slug = sha1(('YmdHis') . str_random(15));
            $model->slugin = Str::uuid();
        });
    }

    public function comments()
    {
        return $this->morphMany(comment::class ,'commentable');
    }

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    public function uploadvideoalable()
    {
        return $this->morphTo();
    }

    public function likes()
    {
        return $this->morphMany(like::class ,'likeable');
    }

    public function viewscount()
    {
        return views($this);
    }

    public function likeked()
    {
        return (bool) like::where('user_id', Auth::id())
            ->where(['likeable_type' => uploadvideo::class,
                'likeable_id' => $this->id ])
            ->first();
    }

    public function favorites()
    {
        return $this->morphMany(favorite::class ,'favoriteable');
    }

    public function favorited()
    {
        return (bool) favorite::where('user_id', Auth::id())
            ->where(['favoriteable_type' => uploadvideo::class,
                'favoriteable_id' => $this->id ])
            ->first();
    }
}
