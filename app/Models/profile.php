<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profile extends Model
{

    protected $table = 'profiles';

    protected $guarded = [
        'created_at','updated_at'
    ];

    protected $casts = [
        'status_comments' => 'boolean',
        'status_like_comments' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    public function country()
    {
        return $this->belongsTo(country::class,'country_id');
    }
}
