<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    use HasFactory;



    public function contactservices()
    {
        return $this->morphMany(contactservice::class ,'contactserviceable')
            ->orderByDesc('created_at');
    }

}
