<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class video extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'videos';

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    public function videotype()
    {
        return $this->belongsTo(videotype::class,'videotype_id');
    }

    public function categoryfilmorserie()
    {
        return $this->belongsTo(categoryfilmorserie::class,'categoryfilmorserie_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
                $model->slug = sha1(('YmdHis') . str_random(15));
                $model->slugin = Str::uuid();
                $model->ip = request()->ip();
            }
        });

        static::updating(function($model){
            $model->ip = request()->ip();
            $model->ip = request()->ip();
        });
    }


    protected $casts = [
        'status' => 'boolean',
    ];

    public function uploadimages()
    {
        return $this->morphMany(uploadimage::class ,'uploadimagealable')
            ->orderByDesc('created_at');
    }

    public function uploadvideos()
    {
        return $this->morphMany(uploadvideo::class ,'uploadvideoalable')
            ->orderByDesc('episode_video');
    }

    public function uploadacteurs()
    {
        return $this->morphMany(uploadacteur::class ,'uploadacteuralable')
            ->orderByDesc('created_at');
    }

    public function likes()
    {
        return $this->morphMany(like::class ,'likeable');
    }

    public function likeked()
    {
        return (bool) like::where('user_id', Auth::id())
            ->where(['likeable_type' => video::class,
                'likeable_id' => $this->id ])
            ->first();
    }

    public function favorites()
    {
        return $this->morphMany(favorite::class ,'favoriteable');
    }

    public function favorited()
    {

        return (bool) favorite::where('user_id', Auth::id())
            ->where(['favoriteable_type' => video::class,
                'favoriteable_id' => $this->id ])
            ->first();
    }

}
