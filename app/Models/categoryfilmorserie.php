<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class categoryfilmorserie extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'categoryfilmorseries';

    protected $casts = [
        'status' => 'boolean',
    ];

    public function videofilms()
    {
        return $this->hasMany(video::class, 'categoryvideo_id');
    }

    public function videoserie()
    {
        return $this->hasMany(video::class, 'categoryvideo_id');
    }
}
