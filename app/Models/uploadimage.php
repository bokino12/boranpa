<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class uploadimage extends Model
{

    protected $fillable = ['link_image','status'];

    protected $table = 'uploadimages';


    protected $casts = [
        'status' => 'boolean',
    ];

    public function videotype()
    {
        return $this->belongsTo(videotype::class,'videotype_id');
    }

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    public function uploadimagealable()
    {
        return $this->morphTo();
    }

    public function likes()
    {
        return $this->morphMany(like::class ,'likeable');
    }

    public function likeked()
    {
        return (bool) like::where('user_id', Auth::id())
            ->where(['likeable_type' => video::class,
                'likeable_id' => $this->id ])
            ->first();
    }
}
