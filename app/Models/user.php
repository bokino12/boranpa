<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Spatie\Permission\Traits\HasRoles;

class user extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user){
            //$user->syncRoles('1');
            $myslug = Str::uuid();
            $user->profile()->create([
                'slug' => $myslug,
            ]);
        });

    }

    public function profile()
    {
        return $this->hasOne(profile::class,'user_id');
    }

    public function videosactors()
    {
        return $this->hasMany(uploadacteur::class, 'acteur_id');
    }

    public function favoritesvideos()
    {
        return $this->hasMany(favorite::class, 'user_id');
    }

    public function favoritesuploadvideos()
    {
        return $this->hasMany(favorite::class, 'user_id');
    }

    public function removelikes()
    {
        return $this->belongsToMany(
            like::class,
            'likes',
            'user_id',
            'likeable_id')
            ->withTimeStamps();
    }

    public function removefavorites()
    {
        return $this->belongsToMany(
            favorite::class,
            'favorites',
            'user_id',
            'favoriteable_id')
            ->withTimeStamps();
    }

}
