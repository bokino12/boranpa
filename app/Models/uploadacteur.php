<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class uploadacteur extends Model
{
    protected $fillable = ['link_video','status','role','acteur_id'];

    protected $table = 'uploadacteurs';

    public function acteur()
    {
        return $this->belongsTo(user::class,'acteur_id');
    }

    protected $casts = [
        'status' => 'boolean',
    ];

    public function uploadacteuralable()
    {
        return $this->morphTo();
    }

    public function likes()
    {
        return $this->morphMany(like::class ,'likeable');
    }

    public function likeked()
    {
        return (bool) like::where('user_id', Auth::guard('web')->id())
            ->where(['likeable_type' => video::class,
                'likeable_id' => $this->id])
            ->first();
    }
}
