<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class responsecomment extends Model
{
    protected $guarded = [];

    protected $table = 'responsecomments';

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    public function to()
    {
        return $this->belongsTo(user::class,'to_id');
    }

    protected $casts = [
        'status' => 'boolean',
    ];

    public function comment()
    {
        return $this->belongsTo(comment::class,'comment_id');
    }


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
                $model->ip = request()->ip();
            }
        });
    }

    public function likes()
    {
        return $this->morphMany(like::class ,'likeable');
    }

    public function likeked()
    {
        return (bool) like::where('user_id', Auth::guard('web')->id())
            ->where(['likeable_type' => responsecomment::class,
                'likeable_id' => $this->id ])
            ->first();
    }


    public function scopeResponseactive($q)
    {
        return $q->where(['status' => 1])->orderByDesc('created_at');
    }
}
