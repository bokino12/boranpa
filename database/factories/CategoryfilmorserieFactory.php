<?php

namespace Database\Factories;

use App\Models\categoryfilmorserie;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class categoryfilmorserieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = categoryfilmorserie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique()->userName;
        return [
            'name' => $name,
            'slug' => str_slug($name),
            'status' => true,
        ];
    }
}
