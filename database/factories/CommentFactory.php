<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\comment;
use App\Models\uploadvideo;
use App\Models\user;
use Faker\Generator as Faker;

$factory->define(comment::class, function (Faker $faker) {
    $servicemodel = collect([
        ['name' => uploadvideo::class],
    ]);

    return [
        'user_id' => user::inRandomOrder()->first()->id,
        'commentable_id' => mt_rand(1, 1000),
        'commentable_type' => $servicemodel->shuffle()->first()['name'],
        'body' => $faker->realText(rand(10, 200)),
        'created_at' => $faker->dateTime,
    ];
});
