<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\uploadacteur;
use App\Models\user;
use App\Models\video;
use Faker\Generator as Faker;

$factory->define(uploadacteur::class, function (Faker $faker) {
    $servicemodel = collect([
        ['name' => video::class],
    ]);

    return [
        'acteur_id' => user::inRandomOrder()->first()->id,
        'role' => $faker->name,
        'uploadacteuralable_id' => mt_rand(1, 300),
        'uploadacteuralable_type' => $servicemodel->shuffle()->first()['name'],
    ];
});
