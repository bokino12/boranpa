<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\videotype;
use App\Models\user;
use App\Models\video;
use Illuminate\Http\File;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

$factory->define(video::class, function (Faker $faker) {
    $backgroundColor = trim($faker->safeHexcolor, '#');
    $foregroundColor = trim($faker->safeHexcolor, '#');
    $title = $faker->firstName;
    return [
        'title' => $title,
        'saison_number' => mt_rand(1, 10),
        'slug' => sha1(('YmdHis') . str_random(15)),
        'slugin' => Str::uuid(),
        'status' => true,
        'years_production' => $faker->year,
        'link_video' => "https://www.youtube.com/watch?v=UVDFhR4Ik8E",
        'link_bande_annonce_video' => "https://www.youtube.com/watch?v=UVDFhR4Ik8E",
        'photo' => "https://dummyimage.com/wsvga/" . $backgroundColor . "/". $foregroundColor ."&text=" . $faker->word,
        'photo_cover' => "https://dummyimage.com/wsvga/" . $backgroundColor . "/". $foregroundColor ."&text=" . $faker->word,
        'description' => $faker->realText(rand(100, 1000)),
        'user_id' => user::inRandomOrder()->first()->id,
        'videotype_id' => videotype::inRandomOrder()->first()->id
    ];
});
