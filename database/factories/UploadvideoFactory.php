<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\acteur;
use App\Models\uploadvideo;
use Illuminate\Http\File;
use App\Models\video;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

$factory->define(uploadvideo::class, function (Faker $faker) {
    $servicemodel = collect([
        ['name' => video::class],
    ]);
    $servicevimodel = collect([
        ['name' => "http://www.w3schools.com/html/mov_bbb.mp4"],
        ['name' => "http://media.w3.org/2010/05/bunny/trailer.mp4"],
        ['name' => "http://media.w3.org/2010/05/bunny/movie.mp4"],
        ['name' => "https://media.w3.org/2010/05/sintel/trailer_hd.mp4"],
    ]);

    $backgroundColor = trim($faker->safeHexcolor, '#');
    $foregroundColor = trim($faker->safeHexcolor, '#');
    return [
        'videotime_red' => mt_rand(20, 60),
        'status' => true,
        'uploadvideoalable_id' => mt_rand(1, 100),
        'uploadvideoalable_type' => $servicemodel->shuffle()->first()['name'],
        'title' => $faker->sentence(3),
        'description' => $faker->realText(rand(10, 200)),
        'episode_video' => mt_rand(1, 100),
        'photo' => "https://dummyimage.com/wsvga/" . $backgroundColor . "/". $foregroundColor ."&text=" . $faker->word,
        'link_video' => $servicevimodel->shuffle()->first()['name'],
        'years_production' => $faker->year,
    ];
});
