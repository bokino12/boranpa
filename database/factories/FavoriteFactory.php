<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\comment;
use App\Models\favorite;
use App\Models\like;
use App\Models\uploadacteur;
use App\Models\uploadimage;
use App\Models\user;
use App\Models\video;
use Faker\Generator as Faker;

$factory->define(favorite::class, function (Faker $faker) {

    $servicemodel = collect([
        ['name' => video::class],
    ]);

    return [
        'user_id' => user::inRandomOrder()->first()->id,
        'favoriteable_id' => mt_rand(1, 300),
        'favoriteable_type' => $servicemodel->shuffle()->first()['name'],
    ];
});
