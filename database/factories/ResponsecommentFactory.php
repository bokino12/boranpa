<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\comment;
use App\Models\responsecomment;
use App\Models\user;
use Faker\Generator as Faker;

$factory->define(responsecomment::class, function (Faker $faker) {
    return [
        'user_id' => user::inRandomOrder()->first()->id,
        'to_id' => user::inRandomOrder()->first()->id,
        'comment_id' => comment::inRandomOrder()->first()->id,
        'body' => $faker->realText(rand(10, 200)),
        'created_at' => $faker->dateTime,
    ];
});
