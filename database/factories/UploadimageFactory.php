<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\boutique;
use App\Models\uploadimage;
use App\Models\user;
use App\Models\video;
use App\Models\videotype;
use Illuminate\Http\File;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

$factory->define(uploadimage::class, function (Faker $faker) {
    $servicemodel = collect([
        ['name' => videotype::class],
        //['name' => boutique::class],
        ['name' => video::class],
    ]);
    $colorsbtn = collect([
        ['name' => 'info'],
        ['name' => 'warning'],
        ['name' => 'danger'],
        ['name' => 'success'],
        ['name' => 'primary'],
    ]);
    $title = $faker->sentence(4);

    $backgroundColor = trim($faker->safeHexcolor, '#');
    $foregroundColor = trim($faker->safeHexcolor, '#');
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'slugin' => Str::uuid(),
        'status' => true,
        'link_image' => video::inRandomOrder()->first()->slug,
        'uploadimagealable_id' => mt_rand(1, 300),
        'uploadimagealable_type' => $servicemodel->shuffle()->first()['name'],
        'videotype_id' => mt_rand(1, 4),
        'user_id' => user::inRandomOrder()->first()->id,
        'color_button' => $colorsbtn->shuffle()->first()['name'],
        'description' => $faker->realText(rand(10, 200)),
        'photo' => "https://dummyimage.com/wsvga/" . $backgroundColor . "/". $foregroundColor ."&text=" . $faker->word,

    ];
});
