<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\comment;
use App\Models\like;
use App\Models\responsecomment;
use App\Models\uploadvideo;
use App\Models\user;
use App\Models\video;
use Faker\Generator as Faker;

$factory->define(like::class, function (Faker $faker) {

    $servicemodel = collect([
        ['name' => uploadvideo::class],
        ['name' => video::class],
        ['name' => comment::class],
        ['name' => responsecomment::class],
    ]);

    return [
        'user_id' => user::inRandomOrder()->first()->id,
        'likeable_id' => mt_rand(1, 900),
        'likeable_type' => $servicemodel->shuffle()->first()['name'],
    ];
});
