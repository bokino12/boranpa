<?php

use App\Models\categoryfilmorserie;
use App\Models\user;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CompileSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $god = user::create([
            'username' =>'bokino12',
            'first_name' =>'Boclair Temgoua',
            'slug' => 'boclair_temgoua',
            'email' => "temgoua2012@gmail.com",
            "password" => bcrypt('0000000'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
        ]);
        //$god->profileadmin()->create([ 'status_user' => true]);
        //$god->syncRoles('super-admin');

        factory(user::class, 3)->create();
        categoryfilmorserie::factory()->count(7)->create();

    }
}
