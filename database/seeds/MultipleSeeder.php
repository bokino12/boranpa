<?php

use App\Models\comment;
use App\Models\favorite;
use App\Models\like;
use App\Models\responsecomment;
use App\Models\uploadacteur;
use App\Models\uploadimage;
use App\Models\uploadvideo;
use App\Models\video;
use Illuminate\Database\Seeder;

class MultipleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(video::class, 30)->create();
        factory(uploadimage::class, 450)->create();
        factory(uploadvideo::class, 1000)->create();
        factory(uploadacteur::class, 500)->create();
        factory(comment::class, 2000)->create();
        factory(responsecomment::class, 3000)->create();
        factory(like::class, 1500)->create();


    }
}
