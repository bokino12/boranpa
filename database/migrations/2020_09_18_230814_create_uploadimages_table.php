<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploadimages', function (Blueprint $table) {
            $table->id();
            $table->string('uploadimagealable_type')->nullable();
            $table->unsignedBigInteger('uploadimagealable_id')->nullable();
            $table->text('photo')->nullable();
            $table->string('slug')->nullable();
            $table->string('slugin')->nullable();
            $table->text('link_image')->nullable();
            $table->string('title')->nullable();
            $table->string('color_button')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();

            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->unsignedBigInteger('videotype_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploadimages');
    }
}
