<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->boolean('status')->nullable();
            $table->longText('description')->nullable();
            $table->string('photo')->nullable();
            $table->string('photo_cover')->nullable();
            $table->unsignedBigInteger('price')->nullable();
            $table->string('slug')->nullable();
            $table->string('slugin')->nullable();
            $table->longText('link_video')->nullable();
            $table->longText('link_bande_annonce_video')->nullable();
            $table->unsignedBigInteger('years_production')->nullable();
            $table->unsignedBigInteger('saison_number')->nullable();
            $table->string('ip')->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('videotype_id')->nullable()->index();
            $table->unsignedBigInteger('categoryfilmorserie_id')->nullable()->index();
            $table->unsignedBigInteger('user_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
