<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadacteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploadacteurs', function (Blueprint $table) {
            $table->id();
            $table->string('uploadacteuralable_type')->nullable();
            $table->unsignedBigInteger('uploadacteuralable_id')->nullable();
            $table->unsignedBigInteger('acteur_id')->nullable()->index();
            $table->string('role')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploadacteurs');
    }
}
