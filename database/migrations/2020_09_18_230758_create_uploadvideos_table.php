<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadvideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploadvideos', function (Blueprint $table) {
            $table->id();
            $table->string('uploadvideoalable_type')->nullable();
            $table->unsignedBigInteger('uploadvideoalable_id')->nullable();
            $table->unsignedBigInteger('years_production')->nullable();
            $table->tinyInteger('episode_video')->nullable();
            $table->boolean('status')->default(false);
            $table->unsignedBigInteger('videotime_red')->nullable();
            $table->string('title')->nullable();
            $table->string('link_video')->nullable();
            $table->longText('link_bande_annonce_video')->nullable();
            $table->string('slug')->nullable();
            $table->string('photo')->nullable();
            $table->string('slugin')->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('saison_number')->nullable();
            $table->string('ip')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploadvideos');
    }
}
