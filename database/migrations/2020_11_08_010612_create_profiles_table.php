<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->boolean('status_comments')->default(true);
            $table->boolean('status_like_comments')->default(false);
            $table->unsignedBigInteger('county_id')->nullable()->index();
            $table->timestamps();

            $table->foreignId('user_id')->nullable()->index()
                ->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
