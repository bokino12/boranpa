<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVideotypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videotypes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('label')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
        });

        $videotypes = array(
            array('name' => 'Films','status' => true, 'label' => 'Ce films','slug' => str_slug('films')),
            array('name' => 'Series','status' => true, 'label' => 'Cette series','slug' => str_slug('series')),
            array('name' => 'Annimés','status' => true, 'label' => 'Cette Annimés','slug' => str_slug('Annimés')),

        );
        DB::table('videotypes')->insert($videotypes);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoryvideos');
    }
}
