<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="76x76" href="/vendor/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/vendor/assets/img/favicon.png">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="/vendor/assets/css/material-kit.css?v=2.2.0" rel="stylesheet" />
    <link href="/vendor/assets/css/video-react.css" rel="stylesheet" />

    <script>
    window.userBoranpa = {!! json_encode([
     'user' => auth()->user(),
     'roles' => auth()->guest() ? : auth()->user()->roles,
     'permissions' => auth()->guest() ? : auth()->user()->getAllPermissions(),
     'guest' => auth()->guest(),
     'authcheck' => auth()->check(),
     'url_site' => htmlspecialchars(config('app.url')),
     'name_site' => htmlspecialchars(config('app.name')),
    ]) !!}
    </script>
</head>
<body>
        @yield('content')

        @routes
        <script src="/vendor/assets/js/core/jquery.min.js" type="text/javascript"></script>
        <script src="/vendor/assets/js/core/popper.min.js" type="text/javascript"></script>
        <script src="/vendor/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="/vendor/assets/js/plugins/moment.min.js"></script>
        <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
        <script src="/vendor/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
        <script src="/vendor/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
        <!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
        <script src="/vendor/assets/js/plugins/jasny-bootstrap.min.js" type="text/javascript"></script>
        <script src="/vendor/assets/js/plugins/bootstrap-notify.js"></script>
        <!--	Plugin for Small Gallery in Product Page -->
        <script src="/vendor/assets/js/plugins/jquery.flexisel.js" type="text/javascript"></script>
        <!-- Place this tag in your head or just before your close body tag. -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
        <script src="/vendor/assets/js/material-kit.js?v=2.2.0" type="text/javascript"></script>
        <!-- Scripts -->
        <script src="/js/site/app.js" defer></script>
</body>
</html>
