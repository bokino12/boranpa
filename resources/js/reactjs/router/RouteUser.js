import React, { Fragment, Suspense } from 'react';
import {Route, Switch,withRouter} from 'react-router-dom';
import IndexSite from "../components/IndexSite";
import AddTodo from "../components/traitement/AddTodo";
import VideotypesiteIndex from "../components/videos/VideotypesiteIndex";
import ContactIndexSite from "../components/pages/ContactIndexSite";
import AboutIndexSite from "../components/pages/AboutIndexSite";
import FaqIndexSite from "../components/pages/FaqIndexSite";
import BoutiqueIndexSite from "../components/videos/BoutiqueIndexSite";
import DetailIndexSite from "../components/videos/DetailIndexSite";
import FavoriteIndexSite from "../components/favorite/FavoriteIndexSite";
import FavoritevideoIndexSite from "../components/favorite/FavoritevideoIndexSite";
import PublicProfileSite from "../components/profile/PublicProfileSite";
import VideoviewSite from "../components/videoshow/VideoviewSite";


const RouteUser = () => (
  <Fragment>
    <Suspense fallback={"wait"}>

        <Switch>

            <Route exact path="/" component={IndexSite}/>
            <Route exact path="/home/" component={IndexSite}/>
            <Route exact path="/store/:videotype/" component={withRouter(VideotypesiteIndex)}/>
            <Route exact path="/home/new_todo" component={AddTodo}/>
            <Route exact path="/favorites/" component={FavoriteIndexSite}/>
            <Route exact path="/favorites/videos/" component={FavoritevideoIndexSite}/>
            <Route exact path="/actors/:user/" component={PublicProfileSite}/>
            <Route exact path="/details/:video/" component={withRouter(DetailIndexSite)}/>
            <Route exact path="/views/:uploadvideo/:video/" component={withRouter(VideoviewSite)} />
            <Route exact path="/boutiques/" component={BoutiqueIndexSite} />
            <Route exact path="/contacts/" component={ContactIndexSite} />
            <Route exact path="/faqs/" component={FaqIndexSite}/>
            <Route exact path="/abouts/" component={AboutIndexSite}/>

      </Switch>

    </Suspense>
</Fragment>

);
export default RouteUser;
