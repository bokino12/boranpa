import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import RouteUser from "./router/RouteUser";
import "../axios"
import 'animate.css/animate.css';
import 'react-toastify/dist/ReactToastify.css';
import ScrollToTop from "./components/inc/ScrollToTop";

/** Redux import*/
import {Provider} from "react-redux";
import store from "./redux/store";
/** On n'oublie pas d'importer les provider qui entoure ici toute notre application*/

/** End*/


class Index extends Component {
    render() {
        return (
            <BrowserRouter>
                <ScrollToTop/>
                <Route component={RouteUser} />
            </BrowserRouter>
        );
    }
}

if (document.getElementById('boranpa')) {
    ReactDOM.render(<Provider store={store}><Index /></Provider>  , document.getElementById('boranpa'));
}
export default Index;
