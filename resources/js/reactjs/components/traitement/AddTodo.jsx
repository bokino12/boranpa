import React, { Component } from "react";
import { Link, NavLink, withRouter } from 'react-router-dom';
import FieldInput from "../inc/FieldInput";


class AddTodo extends Component {
    constructor(props) {
        super(props);

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.saveItem = this.saveItem.bind(this);
        this.state = {
            name: '',
            errors: [],
        };
    

    }

    handleFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
        this.state.errors[event.target.name] = '';
    }
     // Handle Errors
     hasErrorFor(field) {
        return !!this.state.errors[field];
    }

    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            )
        }
    }


    saveItem(e) {
        e.preventDefault();

        let item = {
            name: this.state.name,
        };
        dyaxios.post(route('todos.store'), item)
            .then(() => {
                this.props.history.push(`/home/`);
            }).catch(error => {
                this.setState({
                    errors: error.response.data.errors
                });
            })
    }
   

    render() {
        return (
            <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">                    

                        <div className="card-body">
                        <br/>
                        <form onSubmit={this.saveItem}>
                            <div className="form-group">
                                <label>Name todo</label>
                                <FieldInput name="name" type='text' minLength="4" maxLength="200" placeholder="Name todo" value={this.state.name || ""}
                                                                            handleFieldChange={this.handleFieldChange}
                                                                            hasErrorFor={this.hasErrorFor}
                                                                            required="required"
                                                                            renderErrorFor={this.renderErrorFor}/>
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="row" >

                            <Link to={`/home/`} className="btn btn-default">Annuler</Link>
                            <button type="submit" className="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default AddTodo;
