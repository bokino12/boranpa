import React, {Component, Fragment} from "react";
import { Helmet } from 'react-helmet';
import {Link} from "react-router-dom";
import FieldInput from "./FieldInput";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {loadallvideotypes} from "../../redux/actions/videoActions";


class NavUserSite extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //
        };


    }

    componentDidMount() {
        this.props.loadallvideotypes(this.props);
    }

    render() {
        const {videotypes} = this.props;
        const Mapvideotypes = videotypes.map((item) =>
            <a  key={item.id} href={`/store/${item.slug}/`} className="dropdown-item">
                {item.name}
            </a>
        );
        return (
            <div className="container">
                <div className="navbar-translate">
                    <a className="navbar-brand" href="/">
                        <b>{$name_site}</b>
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="navbar-toggler-icon"></span>
                        <span className="navbar-toggler-icon"></span>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a href="/" className="nav-link">
                                <b>Accueil</b>
                            </a>
                        </li>
                        <li className="dropdown nav-item">
                            <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown">
                                 <b>Catégories</b>
                            </a>
                            <div className="dropdown-menu dropdown-with-icons">
                                {Mapvideotypes}
                            </div>
                        </li>
                        <li className="nav-item">
                            <a href={route('boutiques.index')} className="nav-link">
                                <b>Boutique</b>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a href={route('search.index')} className="nav-link">
                                <i className="material-icons">search</i>
                            </a>
                        </li>
                        {$guest ?
                            <>
                                <li className="nav-item">
                                    <a href={route('login')} className="nav-link">
                                        <b>Connexion</b>
                                    </a>
                                </li>
                                <li className="button-container nav-item iframe-extern">
                                    <a href={route('register')} className="btn  btn-info btn-block">
                                        <b>Inscription</b>
                                    </a>
                                </li>
                            </>
                            :
                            <>
                                <li className="nav-item">
                                    <a href={route('favorites.index')} className="nav-link">
                                        <i className="material-icons">add</i> <b>Ma liste</b>
                                    </a>
                                </li>
                                <li className="dropdown nav-item">
                                    <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown">
                                        <i className="material-icons">supervised_user_circle</i> <b>{$userBoranpa.first_name}</b>
                                    </a>
                                    <div className="dropdown-menu dropdown-with-icons">
                                        <a href="../presentation.html" className="dropdown-item">
                                            Séries
                                        </a>
                                        <a href="../index.html" className="dropdown-item">
                                            Films
                                        </a>
                                        <a href="http://demos.creative-tim.com/material-kit-pro/docs/2.1/getting-started/introduction.html"
                                           className="dropdown-item">
                                            Documentaires
                                        </a>
                                    </div>
                                </li>
                            </>
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

NavUserSite.propTypes = {
    loadallvideotypes: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    videotypes: store.videos.videotypes

});

export default connect(mapStoreToProps,
    { loadallvideotypes,}
)(NavUserSite)
