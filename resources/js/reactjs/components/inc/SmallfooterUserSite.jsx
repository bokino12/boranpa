import React, {Fragment} from "react";
import {Helmet} from 'react-helmet';
import {Link} from "react-router-dom";

export default function SmallfooterUserSite() {
    return (
        <footer className="footer footer-default">
            <div className="container">
                <nav className="float-left">
                    <ul>
                        <li>
                            <Link to={`/`}>
                                {$name_site}
                            </Link>
                        </li>
                        <li>
                            <Link to={`/abouts/`}>
                                About Us
                            </Link>
                        </li>
                        <li>
                            <a href="/">
                                Blog
                            </a>
                        </li>
                        <li>
                            <Link to={`/faqs/`}>
                                Faq
                            </Link>
                        </li>
                        <li>
                            <Link to={`/contacts/`}>
                                Contact
                            </Link>
                        </li>
                    </ul>
                </nav>
                <div className="copyright float-right">
                    &copy; 2020
                    , made with <i className="material-icons">favorite</i> by
                    <a href="/" target="_blank">KazouGroup</a>
                </div>
            </div>
        </footer>
    )
}
