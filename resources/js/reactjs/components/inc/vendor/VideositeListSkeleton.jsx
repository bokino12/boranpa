import React, {PureComponent} from "react";
import Skeleton from "react-loading-skeleton";


class VideositeListSkeleton extends PureComponent {

    render() {
        return (
            <>
                <div className="col-md-10 ml-auto mr-auto">
                    <div className="card card-plain card-blog">
                        <div className="row">
                            <div className="col-md-4">
                                <Skeleton circle={false} height="100%" width="100%" />
                            </div>
                            <div className="col-md-8">
                                <h4 className="card-title">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <Skeleton count={3}  width="100%" />
                                        </div>

                                    </div>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-10 ml-auto mr-auto">
                    <div className="card card-plain card-blog">
                        <div className="row">
                            <div className="col-md-4">
                                <Skeleton circle={false} height="100%" width="100%" />
                            </div>
                            <div className="col-md-8">
                                <h4 className="card-title">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <Skeleton count={3}  width="100%" />
                                        </div>

                                    </div>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-10 ml-auto mr-auto">
                    <div className="card card-plain card-blog">
                        <div className="row">
                            <div className="col-md-4">
                                <Skeleton circle={false} height="100%" width="100%" />
                            </div>
                            <div className="col-md-8">
                                <h4 className="card-title">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <Skeleton count={3}  width="100%" />
                                        </div>

                                    </div>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>

            </>

        )
    }
}

export default VideositeListSkeleton
