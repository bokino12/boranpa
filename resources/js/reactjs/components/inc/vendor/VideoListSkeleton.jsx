import React, {PureComponent} from 'react';
import Skeleton from "react-loading-skeleton";

class VideoListSkeleton extends PureComponent {
    render() {
        return (
            <>
                <div className="col-md-10 ml-auto mr-auto">
                    <div className="card card-plain card-blog">
                        <div className="row">
                            <div className="col-md-4">
                                <Skeleton circle={false} height={260} width="100%" />
                            </div>
                            <div className="col-md-4">
                                <Skeleton circle={false} height={260} width="100%" />
                            </div>
                            <div className="col-md-4">
                                <Skeleton circle={false} height={260} width="100%" />
                            </div>

                        </div>
                    </div>
                </div>

            </>
        );
    }
}

export default VideoListSkeleton;
