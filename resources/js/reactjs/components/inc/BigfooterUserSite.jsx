import React, {Fragment} from "react";
import {Helmet} from 'react-helmet';
import {Link} from "react-router-dom";

export default function BigfooterUserSite() {
    return (
        <footer className="footer footer-black footer-big">
            <div className="container">
                <div className="content">
                    <div className="row">
                        <div className="col-md-4">
                            <h5>About Us</h5>
                            <p>Creative Tim is a startup that creates design tools that make the web development
                                process faster and easier. </p>
                            <p>We love the web and care deeply for how users interact with a digital product. We
                                power businesses and individuals to create better looking web projects around the
                                world. </p>
                        </div>
                        <div className="col-md-4">
                            <h5>Social Feed</h5>
                            <div className="social-feed">
                                <div className="feed-line">
                                    <i className="fa fa-twitter"></i>
                                    <p>How to handle ethical disagreements with your clients.</p>
                                </div>
                                <div className="feed-line">
                                    <i className="fa fa-twitter"></i>
                                    <p>The tangible benefits of designing at 1x pixel density.</p>
                                </div>
                                <div className="feed-line">
                                    <i className="fa fa-facebook-square"></i>
                                    <p>A collection of 25 stunning sites that you can use for inspiration.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <ul className="float-left">
                    <li>
                        <Link to={`/`}>
                            {$name_site}
                        </Link>
                    </li>
                    <li>
                        <Link to={`/abouts/`}>
                            About Us
                        </Link>
                    </li>
                    <li>
                        <a href="/">
                            Blog
                        </a>
                    </li>
                    <li>
                        <Link to={`/faqs/`}>
                            Faq
                        </Link>
                    </li>
                    <li>
                        <Link to={`/contacts/`}>
                            Contact
                        </Link>
                    </li>
                </ul>
                <div className="copyright float-right">
                    Copyright &#xA9;
                    2020
                    Creative Tim All Rights Reserved.
                </div>
            </div>
        </footer>
    )
}
