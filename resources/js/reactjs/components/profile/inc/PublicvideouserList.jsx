import React, {PureComponent} from "react";

import LazyLoad from "react-lazyload";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";
import VideoList from "../../videos/inc/VideoList";


class PublicvideouserList extends PureComponent {


    getDescription() {

        return { __html: (this.props.uploadacteuralable.description.length > 84 ? this.props.uploadacteuralable.description.substring(0, 84) + "..." : this.props.uploadacteuralable.description) };
    }
    render() {
        return (
            <>

                <div className="col-md-6 mx-auto">
                    <div className="card card-background"
                         style={{ backgroundImage: "url(" + this.props.uploadacteuralable.photo + ")" }}>
                        <div className="card-body">
                            <Link to={`/details/${this.props.uploadacteuralable.slug}/`}>
                                <h4 className="card-title">{this.props.uploadacteuralable.title}</h4>
                                {this.props.uploadacteuralable.videotype_id && (
                                    <h6 className="card-category text-info">{this.props.uploadacteuralable.videotype.name}</h6>
                                )}
                                <span className="card-description" dangerouslySetInnerHTML={this.getDescription()}/>
                            </Link>
                            <br/>
                            {this.props.favorited ?
                                <Button onClick={() => this.props.unvsfavoriteItem(this.props)}
                                        className="btn btn-success btn-sm" title="Retirer de vos favoris">
                                    <i className="material-icons">done</i> <b>Ma liste</b>
                                </Button>
                                :
                                <Button onClick={() => this.props.vsfavoriteItem(this.props)}
                                        className="btn btn-rose btn-sm" title="Ajouter à vos favoris">
                                    <i className="material-icons">add</i> <b>Ma liste</b>
                                </Button>
                            }
                            <Link to={`/details/${this.props.uploadacteuralable.slug}/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                            </Link>
                            <h6 className="card-title">Durée: 1 h 30 min - Année: {this.props.uploadacteuralable.years_production}</h6>
                        </div>
                    </div>
                </div>

            </>

        )
    }
}

export default PublicvideouserList
