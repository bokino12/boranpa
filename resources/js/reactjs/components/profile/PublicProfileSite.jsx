import React ,{Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import NavUserSite from "../inc/NavUserSite";
import HelmetSite from "../inc/HelmetSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import {loaduservideos,vsfavoriteItem,unvsfavoriteItem} from "../../redux/actions/favoriteActions";
import {getVideosactorReselect} from "../../redux/selector"
import PublicvideouserList from "./inc/PublicvideouserList";
import VideouserListSkeleton from "../inc/vendor/VideouserListSkeleton";
import VideoList from "../videos/inc/VideoList";


class PublicProfileSite extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


   // Lifecycle Component Method
    componentDidMount() {
        this.props.loaduservideos(this.props);
    }


  render(){
   const {user} = this.props;
      const mapVideos = user.videosactors.length >= 0 ? (
          user.videosactors.map(item => {
              return (
                  <PublicvideouserList key={item.id} {...item} unvsfavoriteItem={this.props.unvsfavoriteItem} vsfavoriteItem={this.props.vsfavoriteItem} />
              )
          })
      ) : (
          <VideouserListSkeleton/>
      );
    return (
        <>
            <div className="about-us sidebar-collapse">

                <HelmetSite title={` ${user.first_name || $name_site } - ${$name_site}`}/>

                <nav
                    className="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-info"
                    color-on-scroll="100" id="sectionsNav">
                    <NavUserSite/>
                </nav>


                <div className="page-header header-filter header-small" data-parallax="true"
                     style={{backgroundImage: "url(" + user.cover_avatar + ")"}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 ml-auto mr-auto text-center">
                                <div className="brand">
                                    <img style={{height: "100px", width: "100px",borderRadius:"100px"}} src={user.avatar}/>
                                    <h2 className="title">
                                         <b>{user.first_name}</b>
                                    </h2>
                                    <p>Je suis dans la place depuis des année le partage et l'echange est ma passuion dans le monde du cinema </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="main main-raised">

                    <div className="section">
                        <div className="container">


                            <div className="row">

                                {mapVideos}

                            </div>

                        </div>
                    </div>
                </div>

                <SmallfooterUserSite/>
            </div>

        </>
    );
  }
}
PublicProfileSite.propTypes = {
    loaduservideos: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    user: getVideosactorReselect(store),

});

export default connect(mapStoreToProps,
    { loaduservideos,vsfavoriteItem,unvsfavoriteItem}
)(PublicProfileSite)

