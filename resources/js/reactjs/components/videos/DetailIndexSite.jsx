import React ,{Component} from 'react';
import PropTypes from "prop-types";
import { Link, NavLink, withRouter } from 'react-router-dom';
import { Button } from "reactstrap";
import ReadMoreAndLess from "react-read-more-less";
import {connect} from "react-redux";
import NavUserSite from "../inc/NavUserSite";
import HelmetSite from "../inc/HelmetSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import {loadvideoshow,unvfavoriteItem,vfavoriteItem,unvlikeItem,vlikeItem} from "../../redux/actions/videoActions";
import ActorsiteIndex from "./inc/ActorsiteIndex";
import VideositeIndex from "../videoshow/VideositeIndex";
import {getVideoReselect} from "../../redux/selector"

class DetailIndexSite extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

   // Lifecycle Component Method
    componentDidMount() {
        this.props.loadvideoshow(this.props);
    }

  render(){
   const {video} = this.props;
    return (
        <>
            <div className="landing-page sidebar-collapse">

                <HelmetSite title={`${video.title || $name_site } - ${$name_site}`}/>

                <nav className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                     color-on-scroll="100" id="sectionsNav">
                    <NavUserSite />
                </nav>

                <div className="page-header header-filter header-small" data-parallax="true"
                     style={{ backgroundImage: "url(" + video.photo || '/vendor/assets/img/blurredimage1.jpg' + ")" }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 text-left">
                                <h3 className="title">{video.title}</h3>
                                <b className="title">{video.saison_number && (`Saison: ${video.saison_number} -`)} Durée: 1 h 25 min - Année: {video.years_production}</b><br/>
                                <span>
                                    <ReadMoreAndLess
                                    className="read-more-content"
                                    charLimit={100}
                                    readMoreText="+"
                                    readLessText="--"
                                >
                                        {video.description || ""}
                                    </ReadMoreAndLess>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="main main-raised">

                    <div className="section">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="buttons">
                                        {/*

                                        <a href="#pablo" className="btn btn-danger">
                                            <i className="material-icons">play_circle_outline</i> <b>Bande-annonce</b>
                                        </a>
                                        */}
                                        <a href="#pablo" className="btn btn-info">
                                            <i className="material-icons">play_circle_filled</i> <b>Lecture</b>
                                        </a>
                                        <a href="#pablo" className="btn btn-danger">
                                            <i className="material-icons">shopping_bag</i> <b>Acheter 3,99 €</b>
                                        </a>

                                        {video.favorited ?
                                            <Button onClick={() => this.props.unvfavoriteItem(video)}
                                                    className="btn btn-success btn-just-icon" title="Retirer de vos favoris">
                                                <i className="material-icons">done</i>
                                            </Button>
                                            :
                                            <Button onClick={() => this.props.vfavoriteItem(video)}
                                                    className="btn btn-rose btn-just-icon" title="Ajouter à vos favoris">
                                                <i className="material-icons">add</i>
                                            </Button>
                                        }

                                        {video.likeked ?
                                            <Button onClick={() => this.props.unvlikeItem(video)}
                                                    className="btn btn-danger btn-just-icon btn-round" title="Je n'adore plus">
                                                <i className="material-icons">favorite</i>
                                            </Button>
                                            :
                                            <Button onClick={() => this.props.vlikeItem(video)}
                                                    className="btn btn-dark btn-just-icon" title="J'adore">
                                                <i className="material-icons">favorite_border</i>
                                            </Button>
                                        }
                                    </div>
                                </div>
                            </div>

                            <VideositeIndex {...this.props}/>

                            <ActorsiteIndex {...this.props} {...video} />

                            {/*
                            <div className="row">
                                <div className="col-md-8 mx-auto text-center">
                                    <h2 className="title">Les clients ont aussi regardé</h2>
                                </div>
                            </div>

                            <div className="row">

                                <div className="col-md-4 mx-auto">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h4 className="card-title">SAMBA Le General ...</h4>
                                            </a>
                                            <p className="card-description">
                                                The truth because we need to restart the human foundation in truth ...
                                            </p>
                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                               <i className="material-icons">add</i> <b>Ma liste</b>
                                            </a>
                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                            </Link>
                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            */}

                        </div>
                    </div>
                </div>

                <SmallfooterUserSite/>
            </div>

        </>
    );
  }
}
DetailIndexSite.propTypes = {
    loadvideoshow: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    video: getVideoReselect(store),
});

export default connect(mapStoreToProps,
    { loadvideoshow,unvfavoriteItem,vfavoriteItem,unvlikeItem,vlikeItem,}
)(DetailIndexSite)
