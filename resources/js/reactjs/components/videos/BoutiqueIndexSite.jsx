import React ,{Component} from 'react';
import PropTypes from "prop-types";
import { Link, NavLink, withRouter } from 'react-router-dom';
import {connect} from "react-redux";
import NavUserSite from "../inc/NavUserSite";
import HelmetSite from "../inc/HelmetSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import {loaduploadimages} from "../../redux/actions/boutiqueActions";
import SlidecarouselIndex from "./inc/SlidecarouselIndex";


class BoutiqueIndexSite extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


   // Lifecycle Component Method
    componentDidMount() {
        this.props.loaduploadimages(this.props);
    }


  render(){
   const {uploadimages} = this.props;
    return (
        <>
            <div className="landing-page sidebar-collapse">

                <HelmetSite title={`Boutiques - ${$name_site}`}/>

                <nav className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                     color-on-scroll="100" id="sectionsNav">
                    <NavUserSite />
                </nav>

                <SlidecarouselIndex {...this.props} {...uploadimages} />

                <div className="main main-raised">

                    <div className="section">
                        <div className="container">

                            <div className="row">

                                <div className="col-md-4 mx-auto">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h4 className="card-title">SAMBA Le General ...</h4>
                                            </a>
                                            <p className="card-description">
                                                The truth because we need to restart the human foundation in truth ...
                                            </p>
                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                                <i className="material-icons">add</i> <b>Ma liste</b>
                                            </a>
                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                            </Link>
                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-4 mx-auto">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color3.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h3 className="card-title">Learn how to use</h3>
                                            </a>
                                            <p className="card-description">
                                                The truth because we need to restart the human foundation  ...
                                            </p>
                                            <a href="#pablo" className="btn btn-success btn-sm" title="Retirer de la liste de favoris">
                                                {/*<i className="material-icons">add</i>*/}
                                                <i className="material-icons">done_all</i> <b>Ma liste</b>
                                            </a>
                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                            </Link>

                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                        </div>
                                    </div>

                                </div>

                                <div className="col-md-4">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color2.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h4 className="card-title">General ...</h4>
                                            </a>
                                            <p className="card-description">
                                                The truth because we need to restart the human foundation in truth ...
                                            </p>
                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                                <i className="material-icons">add</i> <b>Ma liste</b>
                                            </a>
                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                            </Link>
                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-4 mx-auto">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h4 className="card-title">SAMBA Le General ...</h4>
                                            </a>
                                            <p className="card-description">
                                                The truth because we need to restart the human foundation in truth ...
                                            </p>
                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                                <i className="material-icons">add</i> <b>Ma liste</b>
                                            </a>
                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                            </Link>
                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-4 mx-auto">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color3.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h3 className="card-title">Learn how to use</h3>
                                            </a>
                                            <p className="card-description">
                                                The truth because we need to restart the human foundation  ...
                                            </p>
                                            <a href="#pablo" className="btn btn-success btn-sm" title="Retirer de la liste de favoris">
                                                {/*<i className="material-icons">add</i>*/}
                                                <i className="material-icons">done_all</i> <b>Ma liste</b>
                                            </a>
                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                            </Link>

                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                        </div>
                                    </div>

                                </div>



                            </div>

                        </div>
                    </div>
                </div>

                <SmallfooterUserSite/>
            </div>

        </>
    );
  }
}
BoutiqueIndexSite.propTypes = {
    loaduploadimages: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    uploadimages: store.boutiques.uploadimages

});

export default connect(mapStoreToProps,
    { loaduploadimages,}
)(BoutiqueIndexSite)
