import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {
    loadvideotype,loadvideosbyvideotype,vsfavoriteItem,unvsfavoriteItem,
} from "../../redux/actions/videoActions";
import HelmetSite from "../inc/HelmetSite";
import NavUserSite from "../inc/NavUserSite";
import {Link} from "react-router-dom";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import SlidecarouselIndex from "./inc/SlidecarouselIndex";
import VideoList from "./inc/VideoList";
import {getVideosReselect,getVideotypeReselect} from "../../redux/selector"
import VideoListSkeleton from "../inc/vendor/VideoListSkeleton";

class VideotypesiteIndex extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {
        this.props.loadvideotype(this.props);
        this.props.loadvideosbyvideotype(this.props);
    }

    render() {
        const {videotype,videos} = this.props;
        const mapVideos = videos.length >= 0 ? (
            videos.map(item => {
                return (
                    <VideoList key={item.id} {...item} unvsfavoriteItem={this.props.unvsfavoriteItem} vsfavoriteItem={this.props.vsfavoriteItem}  />
                )
            })
        ) : (
            <VideoListSkeleton/>
        );
        return (
           <>
               <div className="landing-page sidebar-collapse">

                   <HelmetSite title={`All videos ${videotype.name} - ${$name_site}`}/>

                   <nav className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                        color-on-scroll="100" id="sectionsNav">
                       <NavUserSite />
                   </nav>

                   <SlidecarouselIndex {...this.props} {...videotype} />

                   <div className="main main-raised">

                       <div className="section">
                           <div className="container">

                               <div className="row">

                                   {mapVideos}

                               </div>

                           </div>
                       </div>
                   </div>

                   <SmallfooterUserSite/>
               </div>
           </>
        );
    }
}

VideotypesiteIndex.propTypes = {
    loadvideotype: PropTypes.func.isRequired,
    loadvideosbyvideotype: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    videotype: getVideotypeReselect(store),
    videos: getVideosReselect(store),

});

export default connect(mapStoreToProps,
    { loadvideotype,loadvideosbyvideotype,vsfavoriteItem,unvsfavoriteItem}
)(VideotypesiteIndex)
