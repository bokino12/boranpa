import React, {PureComponent} from "react";

import LazyLoad from "react-lazyload";
import {Link} from "react-router-dom";


class SlidecarouselIndex extends PureComponent {


    render() {
        const {uploadimages} = this.props;

        return (
            <>

                {uploadimages.length >= 0 ?
                    <>
                        {uploadimages.length < 1 ?

                            <div className="page-header header-filter header-small" data-parallax="true"
                                 style={{backgroundImage: "url(" + '/vendor/assets/img/blurredimage1.jpg' + ")"}}/>

                            :
                            <div id="carouselHomeIndicators" className="carousel slide" data-ride="carousel">
                                {/*
                                <ol className="carousel-indicators">
                                    {uploadimages.map((value,index) => {
                                        return <li key={value.id} data-target="#carouselHomeIndicators" data-slide-to={index} className={index === 0 ? "active" : ""}/>
                                    })}
                                </ol>
                                */}
                                <div className="carousel-inner">
                                    {uploadimages.map((item,index) => (
                                        <div key={item.id} className={`carousel-item ${index === 0 ? "active" : ""}`}>
                                            <Link to={`/details/${item.link_image}/`}>
                                                <div className="page-header header-filter header-small" data-parallax="true"
                                                     style={{ backgroundImage: "url(" + item.photo + ")" }}>
                                                    <div className="container">
                                                        <div className="row">
                                                            <div className="col-md-10 ml-auto mr-auto text-center">
                                                                <h2 className="title">
                                                                    <b>{item.title}</b>
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                    ))}
                                </div>

                                <a className="carousel-control-prev" href="#carouselHomeIndicators" role="button"
                                   data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"/>
                                    <span className="sr-only">Previous</span>
                                </a>
                                <a className="carousel-control-next" href="#carouselHomeIndicators" role="button"
                                   data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"/>
                                    <span className="sr-only">Next</span>
                                </a>

                            </div>
                        }
                    </>
                    :
                    <div className="page-header header-filter header-small" data-parallax="true"
                         style={{backgroundImage: "url(" + '/vendor/assets/img/blurredimage1.jpg' + ")"}}/>
                }

            </>

        )
    }
}

export default SlidecarouselIndex
