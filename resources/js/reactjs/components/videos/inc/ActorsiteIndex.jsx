import React, {PureComponent} from "react";

import LazyLoad from "react-lazyload";
import {Link} from "react-router-dom";


class ActorsiteIndex extends PureComponent {


    render() {
        const {uploadacteurs} = this.props;
        return (
            <>

                {uploadacteurs.length >= 0 ?
                    <>
                        {uploadacteurs.length > 0 &&(
                            <div className="row">
                                <div className="col-md-8 mx-auto text-center">
                                    <h2 className="title">Acteurs principaux</h2>
                                </div>
                            </div>
                        )}


                        <div className="row">
                            {uploadacteurs.map((item) => (
                                <div key={item.id} className="col-sm-3">
                                    <div className="card card-profile card-plain">
                                        <Link to={`/actors/${item.acteur.slug}/`}>
                                            <img style={{ height: "90px", width: "90px",borderRadius:"90px" }}  src={item.acteur.avatar}/>
                                        </Link>
                                        <div className="card-body ">
                                            <h4 className="card-title">{item.acteur.last_name} {item.acteur.first_name}</h4>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </>
                    :
                    null
                }

            </>

        )
    }
}

export default ActorsiteIndex
