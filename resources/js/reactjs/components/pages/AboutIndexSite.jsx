import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Link, NavLink, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import HelmetSite from "./../inc/HelmetSite";
import NavUserSite from "../inc/NavUserSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";


class AboutIndexSite extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <>
                <div className="about-us sidebar-collapse">

                    <HelmetSite title={`Abouts - ${$name_site}`}/>

                    <nav
                        className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                        color-on-scroll="100" id="sectionsNav">
                        <NavUserSite/>
                    </nav>

                    <div className="page-header header-filter header-small" data-parallax="true"
                         style={{backgroundImage: "url(" + '/vendor/assets/img/blurredimage1.jpg' + ")"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 ml-auto mr-auto text-center">
                                    <div className="brand">
                                        <h1 className="title">About page</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="main main-raised">
                        <div className="container">
                            <div className="about-services features-2">
                                <div className="row">
                                    <div className="col-md-8 ml-auto mr-auto text-center">
                                        <h2 className="title">We build awesome products</h2>
                                        <h5 className="description">This is the paragraph where you can write more
                                            details about your product. Keep you user engaged by providing meaningful
                                            information.</h5>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="info info-horizontal">
                                            <div className="icon icon-rose">
                                                <i className="material-icons">gesture</i>
                                            </div>
                                            <div className="description">
                                                <h4 className="info-title">1. Design</h4>
                                                <p>The moment you use Material Kit, you know you&#x2019;ve never felt
                                                    anything like it. With a single use, this powerfull UI Kit lets you
                                                    do more than ever before. </p>
                                                <a href="#pablo">Find more...</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="info info-horizontal">
                                            <div className="icon icon-rose">
                                                <i className="material-icons">build</i>
                                            </div>
                                            <div className="description">
                                                <h4 className="info-title">2. Develop</h4>
                                                <p>Divide details about your product or agency work into parts. Write a
                                                    few lines about each one. A paragraph describing a feature will be
                                                    enough.</p>
                                                <a href="#pablo">Find more...</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="info info-horizontal">
                                            <div className="icon icon-rose">
                                                <i className="material-icons">mode_edit</i>
                                            </div>
                                            <div className="description">
                                                <h4 className="info-title">3. Make Edits</h4>
                                                <p>Divide details about your product or agency work into parts. Write a
                                                    few lines about each one. A paragraph describing a feature will be
                                                    enough.</p>
                                                <a href="#pablo">Find more...</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="about-office">
                                <div className="row text-center">
                                    <div className="col-md-8 ml-auto mr-auto">
                                        <h2 className="title">Our office is our second home</h2>
                                        <h4 className="description">Here are some pictures from our office. You can see
                                            the place looks like a palace and is fully equiped with everything you need
                                            to get the job done.</h4>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office2.jpg"/>
                                    </div>
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office4.jpg"/>
                                    </div>
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office2.jpg"/>
                                    </div>
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office4.jpg"/>
                                    </div>
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office3.jpg"/>
                                    </div>
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office4.jpg"/>
                                    </div>
                                    <div className="col-md-4 mx-auto">
                                        <img className="img-raised rounded img-fluid" alt="Raised Image"
                                             src="/vendor/assets/img/examples/office3.jpg"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <SmallfooterUserSite/>
                </div>
            </>
        );
    }
}

AboutIndexSite.propTypes = {};


export default AboutIndexSite;
