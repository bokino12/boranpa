import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Link, NavLink, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import HelmetSite from "./../inc/HelmetSite";
import NavUserSite from "../inc/NavUserSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import ContactFromIndex from "./treatment/ContactFromIndex";


class ContactIndexSite extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <>
                <div className="ecommerce-page sidebar-collapse">

                    <HelmetSite title={`Contact - ${$name_site}`}/>

                    <nav
                        className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                        color-on-scroll="100" id="sectionsNav">
                        <NavUserSite/>
                    </nav>

                    <div className="page-header header-filter header-small" data-parallax="true"
                         style={{backgroundImage: "url(" + '/vendor/assets/img/blurredimage1.jpg' + ")"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 ml-auto mr-auto text-center">
                                    <div className="brand">
                                        <h1 className="title">Contact</h1>
                                        <h4>Send us a message</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="main main-raised" >
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 mx-auto">
                                    <h2 className="mt-5 text-center">
                                        <b>Get in touch with us</b>
                                    </h2>

                                  <ContactFromIndex/>

                                </div>
                            </div>

                            <br/>
                            <div className="row mr-auto ml-auto mb-5">
                                <div className="col-md-3 col-sm-6">
                                    <div className="icon icon-warning">
                                        <i className="material-icons">pin_drop</i>
                                    </div>
                                    <div className="description">
                                        <h4 className="info-title">Find us at the Office</h4>
                                        <p className="title-grey"> Bld Mihail Kogalniceanu, nr. 8,
                                            <br/> 16134 Genova, Italy
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="icon icon-rose">
                                        <i className="material-icons">phone</i>
                                    </div>
                                    <div className="description">
                                        <h4 className="info-title">Give us a Call</h4>
                                        <p className="title-grey"> +39 3425712192 / +39 3296187465
                                            <br/> Lun - Ven, 8:00 - 22:00
                                        </p>
                                    </div>
                                </div>
                                <div className="service-contact col-md-3 col-sm-6">
                                    <div className="icon icon-info">
                                        <i className="material-icons">mail</i>
                                    </div>
                                    <div className="description">
                                        <h4 className="info-title">E-mail Contact</h4>
                                        <a href="mailto:dasgivemoi@gmail.com" className="title-grey">
                                            <p>{$name_site}@gmail.com</p>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6">
                                    <div className="icon icon-success">
                                        <i className="material-icons">business</i>
                                    </div>
                                    <div className="description">
                                        <h4 className="info-title">Legal Informations</h4>
                                        <p className="title-grey"> {$name_site}
                                            <br/> IVA: 000.000.0000
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <SmallfooterUserSite/>
                </div>
            </>
        );
    }
}

ContactIndexSite.propTypes = {};


export default ContactIndexSite;
