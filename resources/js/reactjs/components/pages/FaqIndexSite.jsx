import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Link, NavLink, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import HelmetSite from "./../inc/HelmetSite";
import NavUserSite from "../inc/NavUserSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import ContactFromIndex from "./treatment/ContactFromIndex";


class FaqIndexSite extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <>
                <div className="about-us sidebar-collapse">

                    <HelmetSite title={`FAQS - ${$name_site}`}/>

                    <nav
                        className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                        color-on-scroll="100" id="sectionsNav">
                        <NavUserSite/>
                    </nav>

                    <div className="page-header header-filter header-small" data-parallax="true"
                         style={{backgroundImage: "url(" + '/vendor/assets/img/blurredimage1.jpg' + ")"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 ml-auto mr-auto text-center">
                                    <div className="brand">
                                        <h1 className="title">FAQS</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="main main-raised">
                        <div className="container">

                            <div className="row">
                                <div className="col-md-10 mx-auto">
                                    <div id="accordion" role="tablist">


                                        <div className="card card-collapse">
                                            <div className="card-header" role="tab" id="headingFaq21">
                                                <h5 className="mb-0">
                                                    <a className="collapsed" data-toggle="collapse"
                                                       href="#collapseFaq21" aria-expanded="false"
                                                       aria-controls="collapseFaq21">
                                                        Es que mes données de carte bancaire son sécuriser?
                                                        <i className="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFaq21" className="collapse" role="tabpanel"
                                                 aria-labelledby="headingFaq21" data-parent="#accordion">
                                                <div className="card-body text-justify">
                                                    <p>Sous la base de notre principe de confidentialité et notre
                                                        licence site, nous vous assurons que vos données sont en
                                                        securité sur notre plateforme tout d'abord pasque:</p>
                                                    <ul>
                                                        <li>Notre site est proteger par la technologie SSH</li>
                                                        <li>Nous avont des assurance</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card card-collapse">
                                            <div className="card-header" role="tab" id="headingFaq21">
                                                <h5 className="mb-0">
                                                    <a className="collapsed" data-toggle="collapse"
                                                       href="#collapseFaq21" aria-expanded="false"
                                                       aria-controls="collapseFaq21">
                                                        Es que mes données de carte bancaire son sécuriser?
                                                        <i className="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFaq21" className="collapse" role="tabpanel"
                                                 aria-labelledby="headingFaq21" data-parent="#accordion">
                                                <div className="card-body text-justify">
                                                    <p>Sous la base de notre principe de confidentialité et notre
                                                        licence site, nous vous assurons que vos données sont en
                                                        securité sur notre plateforme tout d'abord pasque:</p>
                                                    <ul>
                                                        <li>Notre site est proteger par la technologie SSH</li>
                                                        <li>Nous avont des assurance</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card card-collapse">
                                            <div className="card-header" role="tab" id="headingFaq21">
                                                <h5 className="mb-0">
                                                    <a className="collapsed" data-toggle="collapse"
                                                       href="#collapseFaq21" aria-expanded="false"
                                                       aria-controls="collapseFaq21">
                                                        Es que mes données de carte bancaire son sécuriser?
                                                        <i className="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFaq21" className="collapse" role="tabpanel"
                                                 aria-labelledby="headingFaq21" data-parent="#accordion">
                                                <div className="card-body text-justify">
                                                    <p>Sous la base de notre principe de confidentialité et notre
                                                        licence site, nous vous assurons que vos données sont en
                                                        securité sur notre plateforme tout d'abord pasque:</p>
                                                    <ul>
                                                        <li>Notre site est proteger par la technologie SSH</li>
                                                        <li>Nous avont des assurance</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card card-collapse">
                                            <div className="card-header" role="tab" id="headingFaq21">
                                                <h5 className="mb-0">
                                                    <a className="collapsed" data-toggle="collapse"
                                                       href="#collapseFaq21" aria-expanded="false"
                                                       aria-controls="collapseFaq21">
                                                        Es que mes données de carte bancaire son sécuriser?
                                                        <i className="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFaq21" className="collapse" role="tabpanel"
                                                 aria-labelledby="headingFaq21" data-parent="#accordion">
                                                <div className="card-body text-justify">
                                                    <p>Sous la base de notre principe de confidentialité et notre
                                                        licence site, nous vous assurons que vos données sont en
                                                        securité sur notre plateforme tout d'abord pasque:</p>
                                                    <ul>
                                                        <li>Notre site est proteger par la technologie SSH</li>
                                                        <li>Nous avont des assurance</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-8 mx-auto">
                                    <h2 className="mt-5 text-center">
                                        <b>Other Topics</b>
                                    </h2>

                                    <ContactFromIndex/>

                                </div>
                            </div>

                        </div>
                    </div>

                    <SmallfooterUserSite/>
                </div>
            </>
        );
    }
}

FaqIndexSite.propTypes = {};


export default FaqIndexSite;
