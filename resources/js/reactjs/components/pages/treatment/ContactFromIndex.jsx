import React, { Component,Fragment } from "react";
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import {Row, Form, Input, InputGroup, FormGroup, FormText} from 'reactstrap';
import FieldInput from "../../inc/FieldInput";
toast.configure();

class ContactFromIndex extends Component {
    constructor(props) {
        super(props);
        this.state = {
            full_name: '',
            subject: '',
            email: '',
            message: '',
            errors: [],
        };


        this.sendmessageItem = this.sendmessageItem.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
    }


    handleFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
        this.state.errors[event.target.name] = '';
    }

    // Handle Errors
    hasErrorFor(field) {
        return !!this.state.errors[field];
    }

    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            )
        }
    }

    sendmessageItem(e) {
        e.preventDefault();

        let item = {
            email: this.state.email,
            subject: this.state.subject,
            full_name: this.state.full_name,
            message: this.state.message,
        };

        let url = route('contacts.store');
        dyaxios.post(url, item)
            .then(() => {

                toast.dark('🦄 Message envoyé correctement!', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

                this.setState({
                    email: "",
                    full_name: "",
                    subject: "",
                    message: "",
                });
            }).catch(error => {
            this.setState({
                errors: error.response.data.errors
            });
            toast.error('Ooop! Try later please ...', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        })
    }

    render() {
        return (
            <>
            <Form onSubmit={this.sendmessageItem} acceptCharset="UTF-8" >
                <Row>
                    <div className="col-md-4">
                        <FormGroup>
                            <label>Your Full Name</label>
                            <FieldInput name="full_name" type='text' minLength="3" maxLength="50"
                                        placeholder="Ivemo" value={this.state.full_name}
                                        handleFieldChange={this.handleFieldChange}
                                        hasErrorFor={this.hasErrorFor}
                                        renderErrorFor={this.renderErrorFor}/>
                        </FormGroup>
                    </div>
                    <div className="col-md-4">
                        <FormGroup>
                            <label>Your Email</label>
                            <FieldInput name="email" type='email' minLength="3" maxLength="50"
                                        placeholder="ivemo@gmail.com" value={this.state.email}
                                        handleFieldChange={this.handleFieldChange}
                                        hasErrorFor={this.hasErrorFor}
                                        renderErrorFor={this.renderErrorFor}/>
                        </FormGroup>
                    </div>
                    <div className="col-md-4">
                        <FormGroup>
                            <label>Subject</label>
                            <FieldInput name="subject" type='text' minLength="3" maxLength="100"
                                        placeholder="Say hi to you" value={this.state.subject}
                                        handleFieldChange={this.handleFieldChange}
                                        hasErrorFor={this.hasErrorFor}
                                        renderErrorFor={this.renderErrorFor}/>
                        </FormGroup>
                    </div>
                    <div className="col-md-12">
                        <FormGroup>
                            <FieldInput name="message" type='textarea' minLength="5" maxLength="5000"
                                        placeholder="Here you can write your nice text" value={this.state.message}
                                        handleFieldChange={this.handleFieldChange}
                                        hasErrorFor={this.hasErrorFor}
                                        renderErrorFor={this.renderErrorFor} rows="8"/>
                        </FormGroup>
                        <div>
                            <div className="col-md-2 mx-auto">
                                <button type="submit" className="btn btn-danger btn-fill">
                                    Contact Us
                                </button>
                            </div>
                        </div>
                    </div>
                </Row>
            </Form>
                </>
        )
    }
}

export default ContactFromIndex;
