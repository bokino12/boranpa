import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadvideoviewsitehow,uvsfavoriteItem,unuvsfavoriteItem
} from "../../redux/actions/videoActions";
import VideositeListSkeleton from "../inc/vendor/VideositeListSkeleton";
import VideositeshowList from "./inc/VideositeshowList";


class UploadvideositeIndex extends PureComponent {


    componentDidMount() {
        this.props.loadvideoviewsitehow(this.props);
    }

    render() {
        const {videos} = this.props;
        const mapVideos = videos.length >= 0 ? (
            videos.map(item => {
                return (
                    <VideositeshowList key={item.id} {...item} unuvsfavoriteItem={this.props.unuvsfavoriteItem} uvsfavoriteItem={this.props.uvsfavoriteItem}  />
                )
            })
        ) : (
            <VideositeListSkeleton/>
        );
        return (
            <>


                {mapVideos}

            </>

        )
    }
}
UploadvideositeIndex.propTypes = {
    loadvideoviewsitehow: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    videos: store.videos.uploadvideos,
});

export default connect(mapStoreToProps,
    { loadvideoviewsitehow,uvsfavoriteItem,unuvsfavoriteItem}
)(UploadvideositeIndex)
