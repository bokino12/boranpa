import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Link, NavLink, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import HelmetSite from "../inc/HelmetSite";
import NavUserSite from "../inc/NavUserSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import  {Container,Button} from "reactstrap";
import UploadvideositeIndex from "./UploadvideositeIndex";
import {getVideoviewReselect} from "../../redux/selector";
import {loadvideoshow,unuvfavoriteItem,uvfavoriteItem,unuvlikeItem,uvlikeItem,} from "../../redux/actions/uploadvideoActions";
import PlayerVideo from "./inc/PlayerVideo";
import {Row} from "reactstrap";
import Photocover from "../../asset/img/blurredimage1.jpg"
import Commentuploadvideo from "../comment/Commentuploadvideo";

class VideoviewSite extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.loadvideoshow(this.props);
    }

    render() {
        const {video} = this.props;
        return (
            <>
                <div className="about-us sidebar-collapse">

                    <HelmetSite title={`${video.title || $name_site } - ${$name_site}`}/>

                    <nav className="navbar navbar-expand-lg bg-info">
                        <NavUserSite/>
                    </nav>

                    <br/><br/>
                    <div className="main main-raised">
                        <div className="container">

                            <div className="row">
                                <div className="col-md-8 ml-auto mr-auto">
                                    <h4><b>{video.uploadvideoalable.title}</b></h4>
                                    <PlayerVideo {...this.props} {...video} />

                                    <h4 className="card-title">
                                        <div className="row">
                                            <div className="col-sm-8 mx-auto">
                                                {video.episode_video && (`Épisode ${video.episode_video} -`)} {video.title}
                                            </div>
                                            <div className="col-sm-4 mx-auto">

                                                <Row>
                                                    <div className="col-md-6">
                                                        {video.favorited ?
                                                            <Button onClick={() => this.props.unuvfavoriteItem(video)}
                                                                    className="btn btn-success btn-sm" title="Retirer de vos favoris">
                                                                <i className="material-icons">done</i>Ma Liste
                                                            </Button>
                                                            :
                                                            <Button onClick={() => this.props.uvfavoriteItem(video)}
                                                                    className="btn btn-rose btn-sm" title="Ajouter à vos favoris">
                                                                <i className="material-icons">add</i>Ma Liste
                                                            </Button>
                                                        }
                                                    </div>
                                                    <div className="col-md-6">

                                                        {video.likeked ?
                                                            <Button onClick={() => this.props.unuvlikeItem(video)}
                                                                    className="btn btn-info btn-sm" title="Retirer de vos favoris">
                                                                <i className="material-icons">favorite</i> J'aime
                                                            </Button>
                                                            :
                                                            <Button onClick={() => this.props.uvlikeItem(video)}
                                                                    className="btn btn-secondary btn-sm" title="Ajouter à vos favoris">
                                                                <i className="material-icons">favorite_border</i>J'aime
                                                            </Button>
                                                        }

                                                    </div>
                                                </Row>

                                            </div>

                                        </div>
                                    </h4>


                                    <div className="row">
                                        <div className="col-md-12 ml-auto mr-auto">
                                            <h3 className="title text-center">Les clients ont aussi regardé</h3>

                                            <Container>
                                                <Row>
                                                    <div className="col-md-12">
                                                        <div id="carouselExampleIndicatorss"
                                                             className="carousel slide" data-ride="carousel">
                                                            <div className="carousel-inner">

                                                                <div className="carousel-item active">
                                                                    <div className="card card-background"
                                                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                                                        <div className="card-body">
                                                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                                                            <a href="#pablo">
                                                                                <h4 className="card-title">SAMBA Le General ...</h4>
                                                                            </a>
                                                                            <p className="card-description">
                                                                                The truth because we need to restart the human foundation in truth ...
                                                                            </p>
                                                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                                                                <i className="material-icons">add</i> <b>Ma liste</b>
                                                                            </a>
                                                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                                                            </Link>
                                                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="carousel-item">
                                                                    <div className="card card-background"
                                                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                                                        <div className="card-body">
                                                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                                                            <a href="#pablo">
                                                                                <h4 className="card-title">SAMBA Le General ...</h4>
                                                                            </a>
                                                                            <p className="card-description">
                                                                                The truth because we need to restart the human foundation in truth ...
                                                                            </p>
                                                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                                                                <i className="material-icons">add</i> <b>Ma liste</b>
                                                                            </a>
                                                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                                                            </Link>
                                                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="carousel-item">
                                                                    <div className="card card-background"
                                                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                                                        <div className="card-body">
                                                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                                                            <a href="#pablo">
                                                                                <h4 className="card-title">SAMBA Le General ...</h4>
                                                                            </a>
                                                                            <p className="card-description">
                                                                                The truth because we need to restart the human foundation in truth ...
                                                                            </p>
                                                                            <a href="#pablo" className="btn btn-rose btn-sm" title="Retirer de la liste de favoris">
                                                                                <i className="material-icons">add</i> <b>Ma liste</b>
                                                                            </a>
                                                                            <Link to={`/details/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                                                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                                                                            </Link>
                                                                            <h6 className="card-title">Durée: 1 h 30 min - Année: 2019</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a className="carousel-control-prev"
                                                               href="#carouselExampleIndicatorss" role="button"
                                                               data-slide="prev">
                                                                    <span className="carousel-control-prev-icon"
                                                                          aria-hidden="true"></span>
                                                                <span className="sr-only">Previous</span>
                                                            </a>
                                                            <a className="carousel-control-next"
                                                               href="#carouselExampleIndicatorss" role="button"
                                                               data-slide="next">
                                                                    <span className="carousel-control-next-icon"
                                                                          aria-hidden="true"></span>
                                                                <span className="sr-only">Next</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </Row>
                                            </Container>

                                        </div>
                                    </div>


                                    <Commentuploadvideo {...this.props} />

                                </div>
                                <div className="col-md-4 ml-auto mr-auto">

                                    <UploadvideositeIndex {...this.props} />

                                </div>
                            </div>
                            <br/>
                            <div className="row">
                                <div className="col-md-4">

                                </div>
                            </div>

                        </div>
                    </div>

                    <SmallfooterUserSite/>
                </div>
            </>
        );
    }
}

VideoviewSite.propTypes = {
    loadvideoshow: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    video: getVideoviewReselect(store),
});

export default connect(mapStoreToProps,
    { loadvideoshow,unuvfavoriteItem,uvfavoriteItem,unuvlikeItem,uvlikeItem,}
)(VideoviewSite)

