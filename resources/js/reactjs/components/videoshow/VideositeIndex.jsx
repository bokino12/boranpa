import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadvideositehow,
    uvsfavoriteItem,unuvsfavoriteItem,
} from "../../redux/actions/videoActions";
import VideositeList from "./inc/VideositeList";
import VideositeListSkeleton from "../inc/vendor/VideositeListSkeleton";


class VideositeIndex extends PureComponent {


    componentDidMount() {
        this.props.loadvideositehow(this.props);
    }

    render() {
        const {videos} = this.props;
        const mapVideos = videos.length >= 0 ? (
            videos.map(item => {
                return (
                    <VideositeList key={item.id} {...item} unuvsfavoriteItem={this.props.unuvsfavoriteItem} uvsfavoriteItem={this.props.uvsfavoriteItem}  />
                )
            })
        ) : (
            <VideositeListSkeleton/>
        );
        return (
            <>
                {videos.length > 0 && (
                    <div className="row">
                        <div className="col-md-8 mx-auto text-center">
                            <h2 className="title">Épisode de la serie</h2>
                        </div>
                    </div>
                )}


                <div className={'row'}>

                    {mapVideos}

                </div>

            </>

        )
    }
}
VideositeIndex.propTypes = {
    loadvideositehow: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    videos: store.videos.uploadvideos,
});

export default connect(mapStoreToProps,
    { loadvideositehow,uvsfavoriteItem,unuvsfavoriteItem}
)(VideositeIndex)
