import React from 'react';
import {
    BigPlayButton,
    ControlBar,
    ForwardControl,
    Player,
    ReplayControl,
    CurrentTimeDisplay,
    TimeDivider
} from "video-react";

function PlayerVideo(props) {
    return (
        <Player
            autoPlay
            width='100%'
            height='100%'
            playsInline
            poster={props.photo}
            src={props.link_video}>

            <BigPlayButton position="center"/>
            <ControlBar autoHide={true}>
                <ReplayControl seconds={10} order={2.2}/>
                <ForwardControl seconds={10} order={3.2}/>
                <CurrentTimeDisplay order={4.1}/>
                <TimeDivider order={4.2}/>
            </ControlBar>
        </Player>
    )
}

export default PlayerVideo;
