import React, {Fragment, PureComponent} from "react";
import {Link, NavLink} from "react-router-dom";
import {Button} from "reactstrap";
import moment from "moment";

class VideositeshowList extends PureComponent {


    render() {
        return (
            <Fragment key={this.props.id}>
                <br/>
                <div className="row">
                    <div className="col-md-6">
                        <NavLink to={`/views/${this.props.slug}/${this.props.uploadvideoalable.id}/`}>
                            <img className="img"
                                 style={{ height: "100%", width: "100%" }} src={this.props.photo} title={this.props.uploadvideoalable.title} />
                        </NavLink>
                    </div>
                    <div className="col-md-6">
                        <p className="card-title">
                            <Link to={`/views/${this.props.slug}/${this.props.uploadvideoalable.id}/`} title={this.props.title} >
                                Épisode {this.props.episode_video} - {this.props.title.length > 15 ? this.props.title.substring(0, 15) + "..." : this.props.title} </Link>
                            <br/>
                            <b><small>1 h 25 min - {moment(this.props.created_at).fromNow()}</small></b>
                        </p>

                        {this.props.favorited ?
                            <Button onClick={() => this.props.unuvsfavoriteItem(this.props)}
                                    className="btn btn-success btn-just-icon btn-sm" title="Retirer de vos favoris">
                                <i className="material-icons">done</i>
                            </Button>
                            :
                            <Button onClick={() => this.props.uvsfavoriteItem(this.props)}
                                    className="btn btn-rose btn-just-icon btn-sm" title="Ajouter à vos favoris">
                                <i className="material-icons">add</i>
                            </Button>
                        }
                    </div>
                </div>

            </Fragment>

        )
    }
}

export default VideositeshowList
