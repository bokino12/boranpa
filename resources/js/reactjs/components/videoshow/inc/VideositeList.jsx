import React, {PureComponent} from "react";

import LazyLoad from "react-lazyload";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";


class VideositeList extends PureComponent {


    getDescription() {

        return { __html: (this.props.description.length > 80 ? this.props.description.substring(0, 80) + "..." : this.props.description) };
    }
    render() {
        return (
            <>
                <div key={this.props.id} className="col-md-10 ml-auto mr-auto">
                    <div className="card card-plain card-blog">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="card-header card-header-image">
                                    <Link to={`/views/${this.props.slug}/${this.props.uploadvideoalable.id}/`}>
                                        <img className="img"
                                             src={this.props.photo}/>
                                    </Link>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <h4 className="card-title">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <Link to={`/views/${this.props.slug}/${this.props.uploadvideoalable.id}/`}>Épisode {this.props.episode_video} - {this.props.title} </Link>
                                        </div>
                                        <div className="col-md-4">
                                            {this.props.favorited ?
                                                <Button onClick={() => this.props.unuvsfavoriteItem(this.props)}
                                                        className="btn btn-success btn-just-icon btn-sm" title="Retirer de vos favoris">
                                                    <i className="material-icons">done</i>
                                                </Button>
                                                :
                                                <Button onClick={() => this.props.uvsfavoriteItem(this.props)}
                                                        className="btn btn-rose btn-just-icon btn-sm" title="Ajouter à vos favoris">
                                                    <i className="material-icons">add</i>
                                                </Button>
                                            }

                                            <Link to={`/views/${this.props.slug}/${this.props.uploadvideoalable.id}/`} className="btn btn-info btn-just-icon btn-sm" title="Regarder cette video">
                                                <i className="material-icons">play_circle_filled</i>
                                            </Link>

                                            <a href="#" className="btn btn-success btn-just-icon btn-sm" title="Regarder cette video">
                                                <i className="material-icons">edit</i>
                                            </a>
                                        </div>

                                    </div>
                                </h4>

                                <span className="card-description" dangerouslySetInnerHTML={this.getDescription()}/>
                                <p className="author">
                                    <b> 4 septembre 2020 - 1 h 25 min - {this.props.years_production}</b>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </>

        )
    }
}

export default VideositeList
