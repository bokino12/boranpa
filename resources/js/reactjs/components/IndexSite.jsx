import React from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';
import {connect} from "react-redux";
import HelmetSite from "./inc/HelmetSite";
import NavUserSite from "./inc/NavUserSite";
import SmallfooterUserSite from "./inc/SmallfooterUserSite";


const IndexSite = () => {
    return (
        <>
            <div className="landing-page sidebar-collapse">

                <HelmetSite title={`Welcome to - ${$name_site}`}/>

                <nav className="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-info"
                     color-on-scroll="100" id="sectionsNav">
                    <NavUserSite />
                </nav>

                <div className="page-header header-filter header-small" data-parallax="true"
                     style={{ backgroundImage: "url(" + '/vendor/assets/img/blurredimage1.jpg' + ")" }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 ml-auto mr-auto text-center">
                                <div className="brand">
                                    <h1 className="title">Welcome</h1>
                                    <h4>Send us a message</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="main main-raised">

                    <div className="section">
                        <div className="container">

                            <div className="row">

                                <div className="col-md-4">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h3 className="card-title">The best trends in fashion 2017</h3>
                                            </a>
                                            <p className="card-description">
                                                Don't be scared of the truth because we need to restart the human
                                                foundation in truth And I love you like Kanye loves Kanye I love Rick
                                                Owens’ bed design but the back is...
                                            </p>
                                            <a href="#pablo" className="btn btn-white btn-round">
                                                <i className="material-icons">subject</i> Read
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div className="col-md-4">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color3.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Fashion News</h6>
                                            <h3 className="card-title">Kanye joins the Yeezy team at Adidas</h3>
                                            <p className="card-description">
                                                Don't be scared of the truth because we need to restart the human
                                                foundation in truth And I love you like Kanye loves Kanye I love Rick
                                                Owens’ bed design but the back is...
                                            </p>
                                            <a href="#pablo" className="btn btn-white btn-round">
                                                <i className="material-icons">subject</i> Read
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div className="col-md-4">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color2.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Apps</h6>
                                            <a href="#pablo">
                                                <h3 className="card-title">Learn how to use the new colors of 2017</h3>
                                            </a>
                                            <p className="card-description">
                                                Don't be scared of the truth because we need to restart the human
                                                foundation in truth And I love you like Kanye loves Kanye I love Rick
                                                Owens’ bed design but the back is...
                                            </p>
                                            <a href="#pablo" className="btn btn-white btn-round">
                                                <i className="material-icons">subject</i> Read
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div className="col-md-6">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color2.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Tutorials</h6>
                                            <a href="#pablo">
                                                <h3 className="card-title">Trending colors of 2017</h3>
                                            </a>
                                            <p className="card-description">
                                                Don't be scared of the truth because we need to restart the human
                                                foundation in truth And I love you like Kanye loves Kanye I love Rick
                                                Owens’ bed design but the back is...
                                            </p>
                                            <a href="#pablo" className="btn btn-white btn-round">
                                                <i className="material-icons">subject</i> Read
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div className="col-md-6">
                                    <div className="card card-background"
                                         style={{ backgroundImage: "url(" + '/vendor/assets/img/examples/color1.jpg' + ")" }}>
                                        <div className="card-body">
                                            <h6 className="card-category text-info">Productivy Style</h6>
                                            <a href="#pablo">
                                                <h3 className="card-title">Fashion &amp; Style 2017</h3>
                                            </a>
                                            <p className="card-description">
                                                Don't be scared of the truth because we need to restart the human
                                                foundation in truth And I love you like Kanye loves Kanye I love Rick
                                                Owens’ bed design but the back is...
                                            </p>
                                            <a href="#pablo" className="btn btn-white btn-round">
                                                <i className="material-icons">subject</i> read
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <SmallfooterUserSite/>
            </div>

        </>
    );

}
export default IndexSite;
