import React, {Component} from 'react';
import {Form} from "reactstrap";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {loadcommentsuploadvideo,
    likecommentItem,unlikecommentItem,
    likerspcommentItem,unlikerspcommentItem,
    deletecommentItem,deleterspcommentItem}
from "../../redux/actions/commentActions";
import FormComment from "./inc/FormComment";
import ProfileComment from "./inc/ProfileComment";
import {getCommentsReselect} from "../../redux/selector";
import CommentViewList from "./inc/CommentViewList";
import ResponsecommentViewList from "./inc/ResponsecommentViewList";
import {toast} from "react-toastify";
toast.configure();

class Commentuploadvideo extends Component {
    constructor(props) {
        super(props);
        this.responsecommentFromItem = this.responsecommentFromItem.bind(this);
        this.responseresponsecommentFromItem = this.responseresponsecommentFromItem.bind(this);
        this.editcommentFromItem = this.editcommentFromItem.bind(this);
        this.editresponsecommentFromItem = this.editresponsecommentFromItem.bind(this);
        this.cancelresponseCourse = this.cancelresponseCourse.bind(this);
        this.sendcommentItem = this.sendcommentItem.bind(this);
        this.updatecommentItem = this.updatecommentItem.bind(this);
        this.sendresponsecommentItem = this.sendresponsecommentItem.bind(this);
        this.updateresponsecommentItem = this.updateresponsecommentItem.bind(this);
        this.sendresponseresponsecommentItem = this.sendresponseresponsecommentItem.bind(this);

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);

        this.state = {
            body: "",
            editcomment: false,
            responsecomment: false,
            responseresponsecomment: false,
            editresponsecomment:false,
            itemData:[],
            errors:[],
        }
    }

    handleFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
        this.state.errors[event.target.name] = '';
    }

    // Handle Errors
    hasErrorFor(field) {
        return !!this.state.errors[field];
    }

    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            )
        }
    }

    cancelresponseCourse(){
        this.setState({body: "",editcomment: false,responsecomment: false,responseresponsecomment: false,editresponsecomment:false});
    };

    responsecommentFromItem(item) {
        this.setState({
            responsecomment: true,
            id:item.id,
            itemData: item
        });
    }

    responseresponsecommentFromItem(item) {
        this.setState({
            responseresponsecomment: true,
            id:item.id,
            itemData: item
        });
    }

    editcommentFromItem(item) {
        this.setState({
            editcomment: true,
            id:item.id,
            body:item.body,
            itemData: item,
        });
    }

    editresponsecommentFromItem(lk) {
        this.setState({
            editresponsecomment: true,
            id:lk.id,
            body:lk.body,
            itemData: lk
        });
    }


    sendresponseresponsecommentItem(e) {
        e.preventDefault();

        let item = {
            body: this.state.body,
        };

        let Idcommentresponse = this.state.itemData.id;
        let url = route('responsecomments.sendresponse', [Idcommentresponse]);
        dyaxios.post(url, item)
            .then(() => {

                toast.dark('🔝 Comment save successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

                this.setState({body: "",responseresponsecomment: false,});

                this.loadItems();

            }).catch(error => {
            window.location.reload(true);
            this.setState({
                errors: error.response.data.errors
            });
        })
    }

    sendresponsecommentItem(e) {
        e.preventDefault();

        let item = {
            body: this.state.body,
        };

        let itemUploadvideo = this.props.match.params.uploadvideo;
        let itemVideo = this.props.match.params.video;
        let Id = this.state.itemData.id;
        let url = route('uploadvideosendresponsecomment_site',[itemUploadvideo,itemVideo,Id]);
        dyaxios.post(url, item)
            .then(() => {

                toast.dark('🔝 Comment save successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

                this.setState({body: "",responsecomment: false,});

                this.loadItems();

            }).catch(error => {
            window.location.reload(true);
            this.setState({
                errors: error.response.data.errors
            });
        })
    }

    updateresponsecommentItem(e) {
        e.preventDefault();

        let item = {
            body: this.state.body,
        };
        let Idcommentresponse = this.state.itemData.id;
        let url = route('responsecomments.update', [Idcommentresponse]);
        dyaxios.put(url, item)
            .then(response => {

                this.setState({body: "",editresponsecomment: false,});
                this.loadItems();

                toast.dark('🔝 Comment save successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

            }).catch(error => {
            window.location.reload(true);
            this.setState({
                errors: error.response.data.errors
            });
        })
    }

    updatecommentItem(e) {

        e.preventDefault();

        let item = {
            body: this.state.body,
        };
        let itemUploadvideo = this.props.match.params.uploadvideo;
        let itemVideo = this.props.match.params.video;
        let Id = this.state.itemData.id;
        let url = route('uploadvideoupdatecomment_site', [itemUploadvideo,itemVideo,Id]);
        dyaxios.put(url, item)
            .then(response => {

                this.setState({body: "",editcomment: false,});
                this.loadItems();

                toast.dark('🔝 Comment save successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

            }).catch(error => {
            window.location.reload(true);
            this.setState({
                errors: error.response.data.errors
            });
        })
    }

    sendcommentItem(e) {
        e.preventDefault();

        let item = {
            body: this.state.body,
        };

        let itemUploadvideo = this.props.match.params.uploadvideo;
        let itemVideo = this.props.match.params.video;
        let url = route('uploadvideosendcomment_site', [itemUploadvideo, itemVideo]);
        dyaxios.post(url, item)
            .then(() => {

                toast.dark('🔝 Comment save successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

                this.setState({body: "",});

                this.loadItems();

            }).catch(error => {
            window.location.reload(true);
            this.setState({
                errors: error.response.data.errors
            });
        })
    }

    loadItems(){
        this.props.loadcommentsuploadvideo(this.props);
    }

    componentDidMount() {
        this.loadItems()
    }

    render() {
        const {comments} = this.props;
        const {editcomment,editresponsecomment,responsecomment,responseresponsecomment,itemData} = this.state;
        return (
            <>
                <div className="row">
                    <div className="col-md-12 ml-auto mr-auto">

                        {!editcomment && !responsecomment && !editresponsecomment && (
                            <>
                                <h4 className="title text-center">{comments.length} Comments</h4>
                                <div className="media media-post">

                                    <ProfileComment {...this.props} {...$userBoranpa} />

                                    <div className="media-body">
                                        <Form onSubmit={this.sendcommentItem} acceptCharset="UTF-8">

                                            <FormComment value={this.state.body} disabled={!this.state.body} cancelresponseCourse={this.cancelresponseCourse}
                                                         renderErrorFor={this.renderErrorFor} hasErrorFor={this.hasErrorFor}
                                                         handleFieldChange={this.handleFieldChange} namesubmit={`POSTER MON AVIS`}/>

                                        </Form>
                                    </div>
                                </div>
                            </>
                        )}
                        <div className="media-area">

                            {comments.length >= 0  && (

                                <>
                                    {comments.map((item) => (

                                        <div key={item.id} className="media">

                                            <ProfileComment {...this.props} {...item.user} />

                                            <div className="media-body">

                                                {(item.id === itemData.id && editcomment) && (
                                                    <>
                                                        <Form  onSubmit={this.updatecommentItem} acceptCharset="UTF-8">

                                                            <FormComment value={this.state.body} disabled={!this.state.body} cancelresponseCourse={this.cancelresponseCourse}
                                                                         renderErrorFor={this.renderErrorFor} hasErrorFor={this.hasErrorFor}
                                                                         handleFieldChange={this.handleFieldChange} namesubmit={`POSTER MON AVIS`}/>

                                                        </Form>
                                                    </>
                                                )}


                                               <CommentViewList {...this.props}{...item}
                                                                responsecommentFromItem={this.responsecommentFromItem}
                                                                deletecommentItem={this.props.deletecommentItem}
                                                                likecommentItem={this.props.likecommentItem}
                                                                unlikecommentItem={this.props.unlikecommentItem}
                                                                editcommentFromItem={this.editcommentFromItem}
                                               />


                                                {(item.id === itemData.id && responsecomment) && (
                                                    <>
                                                        <Form onSubmit={this.sendresponsecommentItem}  acceptCharset="UTF-8">

                                                            <FormComment value={this.state.body} disabled={!this.state.body} cancelresponseCourse={this.cancelresponseCourse}
                                                                         renderErrorFor={this.renderErrorFor} hasErrorFor={this.hasErrorFor}
                                                                         handleFieldChange={this.handleFieldChange} namesubmit={`POSTER UNE RÉPONSE`}/>

                                                        </Form>

                                                    </>
                                                )}

                                                {item.responsecomments.map((lk) => (

                                                    <div key={lk.id} className="media">

                                                        <ProfileComment {...this.props} {...lk.user} />

                                                        <div className="media-body">

                                                            {(lk.id === itemData.id && editresponsecomment) && (
                                                                <>
                                                                    <Form onSubmit={this.updateresponsecommentItem} acceptCharset="UTF-8">

                                                                        <FormComment value={this.state.body} cancelresponseCourse={this.cancelresponseCourse}
                                                                                     renderErrorFor={this.renderErrorFor} hasErrorFor={this.hasErrorFor}
                                                                                     handleFieldChange={this.handleFieldChange} namesubmit={`POSTER MON AVIS`}/>

                                                                    </Form>
                                                                </>
                                                            )}
                                                            
                                                            <ResponsecommentViewList {...this.props} {...lk}
                                                                                     responseresponsecommentFromItem={this.responseresponsecommentFromItem}
                                                                                     editresponsecommentFromItem={this.editresponsecommentFromItem}
                                                                                     deleterspcommentItem={this.props.deleterspcommentItem}
                                                                                     likerspcommentItem={this.props.likerspcommentItem}
                                                                                     unlikerspcommentItem={this.props.unlikerspcommentItem}
                                                            />

                                                            {(lk.id === itemData.id && responseresponsecomment) && (
                                                                <>
                                                                    <Form  onSubmit={this.sendresponseresponsecommentItem} acceptCharset="UTF-8">

                                                                        <FormComment value={this.state.body} disabled={!this.state.body} cancelresponseCourse={this.cancelresponseCourse}
                                                                                     renderErrorFor={this.renderErrorFor} hasErrorFor={this.hasErrorFor}
                                                                                     handleFieldChange={this.handleFieldChange} namesubmit={`POSTER UNE RÉPONSE`}/>

                                                                    </Form>

                                                                </>
                                                            )}

                                                        </div>
                                                    </div>

                                                ))}

                                            </div>
                                        </div>

                                    ))}
                                </>

                            )}


                        </div>

                    </div>
                </div>
            </>
        );
    }
}
Commentuploadvideo.propTypes = {
    loadcommentsuploadvideo: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    comments: getCommentsReselect(store),
});

export default connect(mapStoreToProps,
    { loadcommentsuploadvideo,
        likecommentItem,unlikecommentItem,
        likerspcommentItem,unlikerspcommentItem,
        deletecommentItem,deleterspcommentItem
    }
)(Commentuploadvideo)
