import React, {PureComponent} from 'react';
import moment from "moment";
import ReadMoreAndLess from "react-read-more-less";
const abbrev = ['', 'k', 'M', 'B', 'T'];

class CommentViewList extends PureComponent {

    data_countfavoritesFormatter(likeked_count, precision) {
        const unrangifiedOrder = Math.floor(Math.log10(Math.abs(likeked_count)) / 3);
        const order = Math.max(0, Math.min(unrangifiedOrder, abbrev.length -1 ));
        const suffix = abbrev[order];
        return (likeked_count / Math.pow(10, order * 3)).toFixed(precision) + suffix;
    }
    render() {
        return (
            <>
                <h4 className="media-heading">
                    {this.props.user.first_name} <small>&#xB7; {moment(this.props.created_at).fromNow()} {this.props.created_at !== this.props.updated_at && ("(Modifié)")}</small></h4>
                <ReadMoreAndLess
                    className="read-more-content"
                    charLimit={120}
                    readMoreText="++"
                    readLessText="--"
                >
                    {this.props.body || ""}
                </ReadMoreAndLess>
                <div className="media-footer">

                    {$guest ?
                        <>
                            <button type="button"
                                    className="btn btn-primary btn-link float-right"
                                    title="Reply to Comment">
                                <i className="material-icons">reply</i> Reply
                            </button>
                        </>
                        :
                        <>
                            {$userBoranpa.id === this.props.user.id && (
                                <>
                                    <button type="button" onClick={() => this.props.deletecommentItem(this.props)}
                                            className="btn btn-danger btn-link float-right"
                                            title="Delete this Comment">
                                        <i className="material-icons">delete_forever</i> Delete
                                    </button>
                                    <button type="button" onClick={() => this.props.editcommentFromItem(this.props)}
                                            className="btn btn-success btn-link float-right"
                                            title="Edit to Comment">
                                        <i className="material-icons">edit</i> Edit
                                    </button>
                                </>
                            )}

                            <button type="button"
                                    onClick={() => this.props.responsecommentFromItem(this.props)}
                                    className="btn btn-primary btn-link float-right"
                                    title="Reply to Comment">
                                <i className="material-icons">reply</i> Reply
                            </button>


                            {this.props.likeked ?
                                <button type="button" onClick={() => this.props.unlikecommentItem(this.props)} className="btn btn-danger btn-link float-right">
                                    <i className="material-icons">favorite</i> {this.props.likeked_count > 0 ? <>{this.data_countfavoritesFormatter(this.props.likeked_count)}</>: ""}
                                </button>
                                :
                                <button type="button" onClick={() => this.props.likecommentItem(this.props)} className="btn btn-link float-right">
                                    <i className="material-icons">favorite</i> {this.props.likeked_count > 0 ? <>{this.data_countfavoritesFormatter(this.props.likeked_count)}</>: ""}
                                </button>
                            }

                        </>
                    }

                </div>
            </>
        );
    }
}

export default CommentViewList;
