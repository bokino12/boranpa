import React from "react";
import FieldInput from "../../inc/FieldInput";

function ProfileComment(props){
    return(
        <div className="avatar">
            <img className="media-object" alt={props.first_name}
                 src={props.avatar}/>
        </div>
    )
}
export default ProfileComment;
