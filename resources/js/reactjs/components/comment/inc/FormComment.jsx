import React from "react";
import FieldInput from "../../inc/FieldInput";

function FormComment(props){
    const {namesubmit,value,disabled,cancelresponseCourse,renderErrorFor,hasErrorFor,handleFieldChange} = props;
    return(
        <>
            <div className="form-group label-floating bmd-form-group">
                <FieldInput name="body" type='textarea' maxLength="5000" placeholder="Laisser votre avis ou un commentaire..."  value={value}
                            handleFieldChange={handleFieldChange}
                            hasErrorFor={hasErrorFor}
                            renderErrorFor={renderErrorFor} rows="3"/>
            </div>
            <div className="media-footer">
                <button type="submit" disabled={disabled}
                        className="btn btn-primary btn-sm btn-wd float-right">
                    <b>{namesubmit}</b>
                </button>

                <button type="button" onClick={cancelresponseCourse}
                        className="btn btn-dark btn-sm btn-wd float-right"> <b>ANNULER</b>
                </button>
            </div>
        </>
    )
}
export default FormComment;
