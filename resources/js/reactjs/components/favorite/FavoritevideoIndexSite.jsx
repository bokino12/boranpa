import React ,{Component} from 'react';
import PropTypes from "prop-types";
import { NavLink } from 'react-router-dom';
import {connect} from "react-redux";
import NavUserSite from "../inc/NavUserSite";
import HelmetSite from "../inc/HelmetSite";
import SmallfooterUserSite from "../inc/SmallfooterUserSite";
import {loaduserfavorites, unvupfavoriteItem} from "../../redux/actions/favoriteActions";
import FavoriteVideoList from "./inc/FavoriteVideoList";
import {getFavoritesReselect} from "../../redux/selector"
import VideoListSkeleton from "../inc/vendor/VideoListSkeleton";


class FavoritevideoIndexSite extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


   // Lifecycle Component Method
    componentDidMount() {
        this.props.loaduserfavorites(this.props);
    }


  render(){
   const {favorite} = this.props;
      const mapFavorites = favorite.favoritesuploadvideos.length >= 0 ? (
          favorite.favoritesuploadvideos.map(item => {
              return (
                  <FavoriteVideoList key={item.id} {...item} unvupfavoriteItem={this.props.unvupfavoriteItem}  />
              )
          })
      ) : (
          <VideoListSkeleton/>
      );
    return (
        <>
            <div className="about-us sidebar-collapse">

                <HelmetSite title={`My favorites - ${$name_site}`}/>

                <nav
                    className="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-info"
                    color-on-scroll="100" id="sectionsNav">
                    <NavUserSite/>
                </nav>


                <div className="page-header header-filter header-small" data-parallax="true"
                     style={{backgroundImage: "url(" + '/vendor/assets/img/examples/color3.jpg' + ")"}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 ml-auto mr-auto text-center">
                                <div className="brand">
                                    <h2 className="title">
                                        <i className="material-icons">add</i> <b>Mes videos favoris</b>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="main main-raised">

                    <div className="section">
                        <div className="container">
                            {favorite.favoritesuploadvideos_count > 0 && (
                                <div className="text-center">
                                    <NavLink to={`/favorites/`} className="btn btn-success btn-sm">
                                        <i className="material-icons">add</i> <b>Ma liste</b>
                                    </NavLink>
                                    <NavLink to={`/favorites/videos/`} className="btn btn-info btn-sm">
                                        <i className="material-icons">play_circle_filled</i> <b>Video</b>
                                    </NavLink>
                                </div>
                            )}

                            <div className="row">

                                {mapFavorites}

                            </div>

                        </div>
                    </div>
                </div>

                <SmallfooterUserSite/>
            </div>

        </>
    );
  }
}
FavoritevideoIndexSite.propTypes = {
    loaduserfavorites: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    favorite: getFavoritesReselect(store),
});

export default connect(mapStoreToProps,
    { loaduserfavorites,unvupfavoriteItem,}
)(FavoritevideoIndexSite)

