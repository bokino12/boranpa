import React, {PureComponent} from "react";

import LazyLoad from "react-lazyload";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";
import VideoList from "../../videos/inc/VideoList";


class FavoriteVideoList extends PureComponent {


    getDescription() {

        return { __html: (this.props.favoriteable.description.length > 84 ? this.props.favoriteable.description.substring(0, 84) + "..." : this.props.favoriteable.description) };
    }
    render() {
        return (
            <>

                <div className="col-md-4">
                    <div className="card card-background"
                         style={{ backgroundImage: "url(" + this.props.favoriteable.photo + ")" }}>
                        <div className="card-body">
                            <Link to={`/views/${this.props.favoriteable.slug}/${this.props.favoriteable.uploadvideoalable.id}/`}>
                                <h4 className="card-title">{this.props.favoriteable.title}</h4>
                                <span className="card-description" dangerouslySetInnerHTML={this.getDescription()}/>
                            </Link>
                            <br/>
                            <Button onClick={() => this.props.unvupfavoriteItem(this.props)}
                                    className="btn btn-success btn-sm" title="Retirer de vos favoris">
                                <i className="material-icons">done</i> <b>Ma liste</b>
                            </Button>
                            <Link to={`/views/${this.props.favoriteable.slug}/${this.props.favoriteable.uploadvideoalable.id}/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                            </Link>
                            <h6 className="card-title">Durée: 1 h 30 min - Année: {this.props.favoriteable.years_production}</h6>
                        </div>
                    </div>
                </div>

            </>

        )
    }
}

export default FavoriteVideoList
