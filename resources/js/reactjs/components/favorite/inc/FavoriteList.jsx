import React, {PureComponent} from "react";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";


class FavoriteList extends PureComponent {


    getDescription() {

        return { __html: (this.props.favoriteable.description.length > 84 ? this.props.favoriteable.description.substring(0, 84) + "..." : this.props.favoriteable.description) };
    }
    render() {
        return (
            <>

                <div className="col-md-4 mx-auto">
                    <div className="card card-background"
                         style={{ backgroundImage: "url(" + this.props.favoriteable.photo + ")" }}>
                        <div className="card-body">
                            <Link to={`/details/${this.props.favoriteable.slug}/`}>
                                <h4 className="card-title">{this.props.favoriteable.title}</h4>
                                {this.props.favoriteable.videotype_id && (
                                    <h6 className="card-category text-info">{this.props.favoriteable.videotype.name}</h6>
                                )}
                                <span className="card-description" dangerouslySetInnerHTML={this.getDescription()}/>
                            </Link>
                            <br/>
                            <Button onClick={() => this.props.unvfavoriteItem(this.props)}
                                    className="btn btn-success btn-sm" title="Retirer de vos favoris">
                                <i className="material-icons">done</i> <b>Ma liste</b>
                            </Button>
                            <Link to={`/details/${this.props.favoriteable.slug}/`} className="btn btn-info btn-sm" title="Regarder cette video">
                                <i className="material-icons">play_circle_filled</i> <b>Regarder</b>
                            </Link>
                            <h6 className="card-title">Durée: 1 h 30 min - Année: {this.props.favoriteable.years_production}</h6>
                        </div>
                    </div>
                </div>

            </>

        )
    }
}

export default FavoriteList
