import {createSelector} from "reselect";

const getVideos = (store) => store.videos.videos;

const getVideoview = (store) => store.uploadvideos.item;

const getVideo = (store) => store.videos.video;

const getVideotype = (store) => store.videos.videotype;

const getFavorites = (store) => store.favorites.favorite;

const getVideosactor = (store) => store.favorites.uservideos;

const getComments = (store) => store.comments.items;


export const getVideosReselect = createSelector([getVideos],
    (getVideos) => {
        return getVideos;
    });

export const getVideoReselect = createSelector([getVideo],
    (getVideo) => {
        return getVideo;
    });

export const getVideoviewReselect = createSelector([getVideoview],
    (getVideoview) => {
        return getVideoview;
    });

export const getVideotypeReselect = createSelector([getVideotype],
    (getVideotype) => getVideotype);

export const getFavoritesReselect = createSelector([getFavorites],
    (getFavorites) => getFavorites);

export const getVideosactorReselect = createSelector([getVideosactor],
    (getVideosactor) => getVideosactor);

export const getCommentsReselect = createSelector([getComments],
    (getComments) => getComments);
