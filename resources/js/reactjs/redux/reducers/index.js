import {combineReducers} from "redux";
import boutiqueReducer from "./boutiqueReducer";
import videoReducer from "./videoReducer";
import favoriteReducer from "./favoriteReducer";
import uploadvideoReducer from "./uploadvideoReducer";
import commentReducer from "./commentReducer";



export default combineReducers({
    videos: videoReducer,
    boutiques: boutiqueReducer,
    favorites: favoriteReducer,
    uploadvideos: uploadvideoReducer,
    comments: commentReducer,
})
