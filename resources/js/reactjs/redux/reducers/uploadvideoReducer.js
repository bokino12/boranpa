import produce from "immer"


const initialState = {
    item:{uploadvideoalable:[]},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_DETAIL_VIDEOSHOW_SHOW_SITE':
                draft.item = action.payload;
                return;

            case 'FAVORITE_UPLOADVIDEO_ADD':
                draft.item.favorited = action.payload;
                return;

            case 'FAVORITE_UPLOADVIDEO_REMOVE':
                draft.item.favorited = !action.payload;
                return;

            case 'UPLOADVIDEO_LIKE_ADD':
                draft.item.likeked = action.payload;
                return;

            case 'UPLOADVIDEO_UNLIKE_REMONE':
                draft.item.likeked = !action.payload;
                return;

        }
    },
    initialState
)

