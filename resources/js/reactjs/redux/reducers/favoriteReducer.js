import produce from "immer"


const initialState = {
    favorite:{favoritesvideos:{},favoritesuploadvideos:{}},
    uservideos:{videosactors:{ uploadacteuralable:[],acteur:[]}},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_USERVIDEOS_PUBLIC_SITE':
                draft.uservideos = action.payload;
                return;

            case 'GET_FAVORITES_VIDEOS_SITE':
                draft.favorite = action.payload;
                return;

            case 'DELETE_UPLOADVIDEOS_FAVORITE':
                let datavideoupdelete = draft.favorite.favoritesuploadvideos.findIndex(i => i.id === action.payload);
                if (datavideoupdelete !== -1) draft.favorite.favoritesuploadvideos.splice(datavideoupdelete, 1);
                if (datavideoupdelete !== -1) draft.favorite.favoritesuploadvideos_count--;
                return draft;

            case 'DELETE_VIDEO_FAVORITE':
                let datavideodelete = draft.favorite.favoritesvideos.findIndex(i => i.id === action.payload);
                if (datavideodelete !== -1) draft.favorite.favoritesvideos.splice(datavideodelete, 1);
                if (datavideodelete !== -1) draft.favorite.favoritesvideos_count--;
                return draft;

        }
    },
    initialState
)

