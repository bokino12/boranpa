import produce from "immer"


const initialState = {
    videotypes:[],
    videos:{uploadacteurs:{acteur:[]},videotype:[]},
    uploadvideos:{},
    videotype:{uploadimages:[]},
    video:{uploadacteurs:{acteur:[]},uploadvideos:[]},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_VIDEOS_BY_VIDEOTYPE_SITE':
                draft.videos = action.payload;
                return;

            case 'GET_VIDEOS_FOR_DETAIL_VIDEOSHOW_SITE':
                draft.uploadvideos = action.payload;
                return;

            case 'GET_DETAIL_VIDEOSHOW_SITE':
                draft.video = action.payload;
                return;

            case 'VIDEO_FAVORITE_ADD':
                draft.video.favorited = action.payload;
                return;

            case 'VIDEO_UNFAVORITE_REMONE':
                draft.video.favorited = !action.payload;
                return;

            case 'VIDEO_LIKE_ADD':
                draft.video.likeked = action.payload;
                return;

            case 'VIDEO_UNLIKE_REMONE':
                draft.video.likeked = !action.payload;
                return;

            case 'GET_VIDEOS_FOR_DETAIL_VIDEOSHOW_SHOW_SITE':
                draft.uploadvideos = action.payload;
                return;

            case 'FAVORITE_UPLOADVIDEOS_ADD':
                let datupdd = draft.uploadvideos.findIndex(i => i.slugin === action.payload);
                if (datupdd !== -1) draft.uploadvideos[datupdd].favorited = action.payload;
                return draft;

            case 'FAVORITE_UPLOADVIDEOS_REMOVE':
                let datupremove = draft.uploadvideos.findIndex(i => i.slugin === action.payload);
                if (datupremove !== -1) draft.uploadvideos[datupremove].favorited = !action.payload;
                return draft;

            case 'FAVORITE_VIDEOS_ADD':
                let datadd = draft.videos.findIndex(i => i.slugin === action.payload);
                if (datadd !== -1) draft.videos[datadd].favorited = action.payload;
                return draft;

            case 'FAVORITE_VIDEOS_REMOVE':
                let dataremove = draft.videos.findIndex(i => i.slugin === action.payload);
                if (dataremove !== -1) draft.videos[dataremove].favorited = !action.payload;
                return draft;

            case 'GET_VIDEOTYPES_SITE':
                draft.videotypes = action.payload;
                return;

            case 'VIDEOTYPE_SITE':
                draft.videotype = action.payload;
                return;

        }
    },
    initialState
)

