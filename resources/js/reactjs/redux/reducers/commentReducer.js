import produce from "immer"


const initialState = {
    items:{responsecomments:[],user:[]},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_COMMENT_UPLOADVIDEO_SITE':
                draft.items = action.payload;
                return;

            case 'COMMENT_LIKE_ADD':
                let datalikec = draft.items.findIndex(i => i.id === action.payload);
                if (datalikec !== -1) draft.items[datalikec].likeked = action.payload;
                if (datalikec !== -1) draft.items[datalikec].likeked_count ++;
                return draft;

            case 'COMMENT_LIKE_REMOVE':
                let dataunlikec = draft.items.findIndex(i => i.id === action.payload);
                if (dataunlikec !== -1) draft.items[dataunlikec].likeked = !action.payload;
                if (dataunlikec !== -1) draft.items[dataunlikec].likeked_count --;
                return draft;

            case 'RESPONSE_COMMENT_LIKE_ADD':
                let datalikerspc = draft.items.findIndex(i => i.id === action.payload.comment_id);
                let datalikerspc1 = draft.items[datalikerspc].responsecomments.findIndex(i => i.id === action.payload.id);
                if (datalikerspc1 !== -1) draft.items[datalikerspc].responsecomments[datalikerspc1].likeked = action.payload;
                if (datalikerspc1 !== -1) draft.items[datalikerspc].responsecomments[datalikerspc1].likeked_count ++;
                return draft;

            case 'RESPONSE_COMMENT_LIKE_REMOVE':
                let dataunlikerspc = draft.items.findIndex(i => i.id === action.payload.comment_id);
                let dataunlikerspc1 = draft.items[dataunlikerspc].responsecomments.findIndex(i => i.id === action.payload.id);
                if (dataunlikerspc1 !== -1) draft.items[dataunlikerspc].responsecomments[dataunlikerspc1].likeked = !action.payload;
                if (dataunlikerspc1 !== -1) draft.items[dataunlikerspc].responsecomments[dataunlikerspc1].likeked_count --;
                return draft;

            case 'DELETE_COMMENT':
                let datacmdelete = draft.items.findIndex(i => i.id === action.payload);
                if (datacmdelete !== -1) draft.items.splice(datacmdelete, 1);
                return draft;

            case 'DELETE_RESPONSE_COMMENT':
                let datarspcmdelete = draft.items.findIndex(i => i.id === action.payload.comment_id);
                let datarspcmdelete1 = draft.items[datarspcmdelete].responsecomments.findIndex(i => i.id === action.payload.id);
                if (datarspcmdelete1 !== -1) draft.items[datarspcmdelete].responsecomments.splice(datarspcmdelete1, 1);
                return draft;

        }
    },
    initialState
)

