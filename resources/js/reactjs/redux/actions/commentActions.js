import {
    GET_COMMENT_UPLOADVIDEO_SITE,
    COMMENT_LIKE_ADD, COMMENT_LIKE_REMOVE,
    RESPONSE_COMMENT_LIKE_ADD, RESPONSE_COMMENT_LIKE_REMOVE,
    DELETE_COMMENT, DELETE_RESPONSE_COMMENT
} from "./index";

import Swal from "sweetalert2";
import {toast} from "react-toastify";
toast.configure();


export const loadcommentsuploadvideo = (props) => dispatch => {

    let itemUploadvideo = props.match.params.uploadvideo;
    let itemVideo = props.match.params.video;
    let url = route('api.uploadvideo_comments', [itemUploadvideo, itemVideo]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_COMMENT_UPLOADVIDEO_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const likecommentItem = (props) => dispatch => {

    const url = route('comments_likes.active', [props.id]);
    dyaxios.post(url).then(() => {
            dispatch({
                type: COMMENT_LIKE_ADD,
                payload: props.id
            });

        }
    ).catch(error => console.error(error));
};

export const unlikecommentItem = (props) => dispatch => {

    const url = route('comments_likes.unactive', [props.id]);
    dyaxios.post(url).then(() => {
            dispatch({
                type: COMMENT_LIKE_REMOVE,
                payload: props.id
            });
        }
    ).catch(error => console.error(error));
};

export const likerspcommentItem = (props) => dispatch => {

    const url = route('responsecomments_likes.active', [props.id]);
    dyaxios.post(url).then(() => {
            dispatch({
                type: RESPONSE_COMMENT_LIKE_ADD,
                payload: props
            });
        }
    ).catch(error => console.error(error));
};

export const unlikerspcommentItem = (props) => dispatch => {

    const url = route('responsecomments_likes.unactive', [props.id]);
    dyaxios.post(url).then(() => {
            dispatch({
                type: RESPONSE_COMMENT_LIKE_REMOVE,
                payload: props
            });

        }
    ).catch(error => console.error(error));
};

export const deletecommentItem = props => dispatch => {

    Swal.fire({
        title: 'Remove this comment',
        text: "are you sure you want to perform this action",
        type: 'warning',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Oui, confirm',
        cancelButtonText: 'Non, cancel',
        showCancelButton: true,
        reverseButtons: true,
    }).then((result) => {
        if (result.value) {

            const url = route('comments.destroy', [props.id]);
            //Envoyer la requet au server
            dyaxios.delete(url).then(() => {

                dispatch({
                    type: DELETE_COMMENT,
                    payload: props.id
                });

                toast.dark('🚮 Comment deleted successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });


            }).catch(error => console.error(error));
        }
    });

};

export const deleterspcommentItem = props => dispatch => {

    Swal.fire({
        title: 'Remove this comment',
        text: "are you sure you want to perform this action",
        type: 'warning',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Oui, confirm',
        cancelButtonText: 'Non, cancel',
        showCancelButton: true,
        reverseButtons: true,
    }).then((result) => {
        if (result.value) {

            const url = route('responsecomments.destroy', [props.id]);
            //Envoyer la requet au server
            dyaxios.delete(url).then(() => {

                dispatch({
                    type: DELETE_RESPONSE_COMMENT,
                    payload: props
                });

                toast.dark('🚮 Comment deleted successfully !', {
                    position: "bottom-left",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

            }).catch(error => console.error(error));
        }
    });

};
