import {
    GET_SLIDE_IMAGEBOUTIQUE_SITE,
} from "./index";

import Swal from "sweetalert2";




export const loaduploadimages = (props) => dispatch => {

    let url = route('api.slideboutique_side');
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_SLIDE_IMAGEBOUTIQUE_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};
