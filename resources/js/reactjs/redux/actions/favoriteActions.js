import {
    GET_FAVORITES_VIDEOS_SITE,
    GET_USERVIDEOS_PUBLIC_SITE,
    DELETE_VIDEO_FAVORITE,
    DELETE_UPLOADVIDEOS_FAVORITE,
} from "./index";

import Swal from "sweetalert2";




export const loaduserfavorites = (props) => dispatch => {

    let url = route('api.favorites_site');
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_FAVORITES_VIDEOS_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const loaduservideos = (props) => dispatch => {

    let itemUser = props.match.params.user;
    let url = route('api.actors_site',[itemUser]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_USERVIDEOS_PUBLIC_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const unvupfavoriteItem = props => dispatch => {

    Swal.fire({
        title: 'Remove this video',
        text: "are you sure you want to perform this action",
        type: 'warning',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Oui, confirm',
        cancelButtonText: 'Non, cancel',
        showCancelButton: true,
        reverseButtons: true,
    }).then((result) => {
        if (result.value) {

            const url = route('videos_userunfavorites.unactive', [props.id]);
            //Envoyer la requet au server
            dyaxios.post(url).then(() => {

                dispatch({
                    type: DELETE_UPLOADVIDEOS_FAVORITE,
                    payload: props.id
                });


            }).catch(error => console.error(error));
        }
    });

};

export const unvfavoriteItem = props => dispatch => {

    Swal.fire({
        title: 'Remove this video',
        text: "are you sure you want to perform this action",
        type: 'warning',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Oui, confirm',
        cancelButtonText: 'Non, cancel',
        showCancelButton: true,
        reverseButtons: true,
    }).then((result) => {
        if (result.value) {

            const url = route('videos_userunfavorites.unactive', [props.id]);
            //Envoyer la requet au server
            dyaxios.post(url).then(() => {

                dispatch({
                    type: DELETE_VIDEO_FAVORITE,
                    payload: props.id
                });


            }).catch(error => console.error(error));
        }
    });

};
