import {
    FAVORITE_UPLOADVIDEO_ADD,
    FAVORITE_UPLOADVIDEO_REMOVE,
    GET_DETAIL_VIDEOSHOW_SHOW_SITE,
    UPLOADVIDEO_LIKE_ADD, UPLOADVIDEO_UNLIKE_REMONE,
} from "./index";

import Swal from "sweetalert2";
import {toast} from "react-toastify";
toast.configure();

export const loadvideoshow = (props) => dispatch => {

    let itemUploadvideo = props.match.params.uploadvideo;
    let itemVideo = props.match.params.video;
    let url = route('api.video_view_site', [itemUploadvideo, itemVideo]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_DETAIL_VIDEOSHOW_SHOW_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const uvfavoriteItem = (props) => dispatch => {

    const url = route('upvideos_favorites.active', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: FAVORITE_UPLOADVIDEO_ADD,
                payload: props.slugin
            });
            toast.dark('💖 Video add to favorite', {
                position: "bottom-left",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    ).catch(error => console.error(error));
};

export const unuvfavoriteItem = (props) => dispatch => {

    const url = route('upvideos_favorites.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: FAVORITE_UPLOADVIDEO_REMOVE,
                payload: props.slugin
            });
            toast.dark('💔 Video remove to favorite', {
                position: "bottom-left",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    ).catch(error => console.error(error));
};

export const uvlikeItem = (props) => dispatch => {

    const url = route('upvideos_likes.active', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: UPLOADVIDEO_LIKE_ADD,
                payload: props.slugin
            });
        }
    ).catch(error => console.error(error));
};

export const unuvlikeItem = (props) => dispatch => {

    const url = route('upvideos_likes.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: UPLOADVIDEO_UNLIKE_REMONE,
                payload: props.slugin
            });

        }
    ).catch(error => console.error(error));
};

