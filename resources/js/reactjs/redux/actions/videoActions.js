import {
    VIDEOTYPE_SITE,
    VIDEO_LIKE_ADD,
    VIDEO_UNLIKE_REMONE,
    GET_VIDEOS_BY_VIDEOTYPE_SITE,
    GET_DETAIL_VIDEOSHOW_SITE,
    GET_VIDEOTYPES_SITE,
    GET_VIDEOS_FOR_DETAIL_VIDEOSHOW_SITE,
    VIDEO_FAVORITE_ADD,
    VIDEO_UNFAVORITE_REMONE,
    FAVORITE_VIDEOS_ADD,
    FAVORITE_VIDEOS_REMOVE,
    FAVORITE_UPLOADVIDEOS_ADD,
    FAVORITE_UPLOADVIDEOS_REMOVE, GET_VIDEOS_FOR_DETAIL_VIDEOSHOW_SHOW_SITE,
} from "./index";

import Swal from "sweetalert2";
import {toast} from "react-toastify";

toast.configure();

export const loadvideoshow = (props) => dispatch => {

    let itemVideo = props.match.params.video;
    let url = route('api.videodetailshow_site', [itemVideo]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_DETAIL_VIDEOSHOW_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const loadvideositehow = (props) => dispatch => {

    let itemVideo = props.match.params.video;
    let url = route('api.videositeshow_site', [itemVideo]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_VIDEOS_FOR_DETAIL_VIDEOSHOW_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const loadvideoviewsitehow = (props) => dispatch => {

    let itemUploadvideo = props.match.params.uploadvideo;
    let itemVideo = props.match.params.video;
    let url = route('api.videosvideo_view_site', [itemUploadvideo, itemVideo]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_VIDEOS_FOR_DETAIL_VIDEOSHOW_SHOW_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const loadvideosbyvideotype = (props) => dispatch => {

    let itemVideotype = props.match.params.videotype;
    let url = route('api.videobyvideotype_site', [itemVideotype]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_VIDEOS_BY_VIDEOTYPE_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const loadvideotype = (props) => dispatch => {

    let itemVideotype = props.match.params.videotype;
    let url = route('api.showvideotype_site', [itemVideotype]);
    dyaxios.get(url).then(response =>
        dispatch({
            type: VIDEOTYPE_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};


export const loadallvideotypes = (props) => dispatch => {

    let url = route('api.videotypes_site');
    dyaxios.get(url).then(response =>
        dispatch({
            type: GET_VIDEOTYPES_SITE,
            payload: response.data
        })
    ).catch(error => console.error(error));
};

export const vfavoriteItem = (props) => dispatch => {

    const url = route('videos_favorites.active', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: VIDEO_FAVORITE_ADD,
                payload: props.slugin
            });

            toast.dark('🦄 Video add to favorite', {
                position: "bottom-left",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    ).catch(error => console.error(error));
};

export const unvfavoriteItem = (props) => dispatch => {

    const url = route('videos_favorites.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: VIDEO_UNFAVORITE_REMONE,
                payload: props.slugin
            });

            toast.dark('🦄 Video remove to favorite', {
                position: "bottom-left",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    ).catch(error => console.error(error));
};

export const vsfavoriteItem = (props) => dispatch => {

    const url = route('videos_favorites.active', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: FAVORITE_VIDEOS_ADD,
                payload: props.slugin
            });
        }
    ).catch(error => console.error(error));
};

export const unvsfavoriteItem = (props) => dispatch => {

    const url = route('videos_favorites.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: FAVORITE_VIDEOS_REMOVE,
                payload: props.slugin
            });
        }
    ).catch(error => console.error(error));
};

export const uvsfavoriteItem = (props) => dispatch => {

    const url = route('upvideos_favorites.active', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: FAVORITE_UPLOADVIDEOS_ADD,
                payload: props.slugin
            });
            toast.dark('🦄 Video add to favorite', {
                position: "bottom-left",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    ).catch(error => console.error(error));
};

export const unuvsfavoriteItem = (props) => dispatch => {

    const url = route('upvideos_favorites.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: FAVORITE_UPLOADVIDEOS_REMOVE,
                payload: props.slugin
            });

            toast.dark('🦄 Video remove to favorite', {
                position: "bottom-left",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    ).catch(error => console.error(error));
};

export const vlikeItem = (props) => dispatch => {

    const url = route('videos_likes.active', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: VIDEO_LIKE_ADD,
                payload: props.slugin
            });
        }
    ).catch(error => console.error(error));
};

export const unvlikeItem = (props) => dispatch => {

    const url = route('videos_likes.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

            dispatch({
                type: VIDEO_UNLIKE_REMONE,
                payload: props.slugin
            });
        }
    ).catch(error => console.error(error));
};
