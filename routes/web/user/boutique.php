<?php

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'slide_boutiques',
        'BoutiqueController@apislideboutique'
    )->name('api.slideboutique_side');

});


Route::get(
    'boutiques',
    'BoutiqueController@boutique'
)->name('boutiques.index');

