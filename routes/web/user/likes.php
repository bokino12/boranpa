<?php

Route::post(
    'videos_likes/{video:slugin}/likes',
    'LikeController@likevideo'
)->name('videos_likes.active');

Route::post(
    'videos_unlikes/{video:slugin}/unlikes',
    'LikeController@unlikevideo'
)->name('videos_likes.unactive');

Route::post(
    'upvideos_likes/{uploadvideo:slugin}/likes',
    'LikeController@likeupvideo'
)->name('upvideos_likes.active');

Route::post(
    'upvideos_unlikes/{uploadvideo:slugin}/unlikes',
    'LikeController@unlikeupvideo'
)->name('upvideos_likes.unactive');

Route::post(
    'comments_likes/{comment:id}/likes',
    'LikeController@likecomment'
)->name('comments_likes.active');

Route::post(
    'comments_unlikes/{comment:id}/unlikes',
    'LikeController@unlikecomment'
)->name('comments_likes.unactive');

Route::post(
    'responsecomments_likes/{responsecomment:id}/likes',
    'LikeController@likerspcomment'
)->name('responsecomments_likes.active');

Route::post(
    'responsecomments_unlikes/{responsecomment:id}/unlikes',
    'LikeController@unlikerspcomment'
)->name('responsecomments_likes.unactive');



