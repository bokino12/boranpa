<?php

Route::get(
    'api/favorites',
    'FavoriteController@apifavorites'
)->name('api.favorites_site');

Route::get(
    'favorites',
    'FavoriteController@index'
)->name('favorites.index');

Route::get(
    'favorites/videos',
    'FavoriteController@videos'
)->name('favorites.videos');

Route::post(
    'videos_favorites/{video:slugin}/favorite',
    'FavoriteController@favoritevideo'
)->name('videos_favorites.active');

Route::post(
    'videos_unfavorites/{video:slugin}/unfavorites',
    'FavoriteController@unfavoritevideo'
)->name('videos_favorites.unactive');

Route::post(
    'upvideos_favorites/{uploadvideo:slugin}/favorite',
    'FavoriteController@favoriteupvideo'
)->name('upvideos_favorites.active');

Route::post(
    'upvideos_unfavorites/{uploadvideo:slugin}/unfavorites',
    'FavoriteController@unfavoriteupvideo'
)->name('upvideos_favorites.unactive');

Route::post(
    'videos_userunfavorites/{favorite:id}/unfavorites',
    'FavoriteController@userunfavoritevideo'
)->name('videos_userunfavorites.unactive');


