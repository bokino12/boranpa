<?php


Route::group(['prefix' => 'api'], function () {

    Route::get(
        'videotypes',
        'VideoController@apivideotypes'
    )->name('api.videotypes_site');

    Route::get(
        'slide/{videotype:slug}/slide',
        'VideoController@apivideotypeslide'
    )->name('api.showvideotype_site');

    Route::get(
        'store/{videotype:slug}',
        'VideoController@apivideobyvideotype'
    )->name('api.videobyvideotype_site');

    Route::get(
        'details/{video:slug}',
        'VideoController@apivideoshow'
    )->name('api.videodetailshow_site');

    Route::get(
        'details/{video:slug}/videos',
        'VideoController@apivideositeshow'
    )->name('api.videositeshow_site');

    Route::get(
        'views/{uploadvideo:slug}/{video:id}',
        'VideoController@apiviewshow'
    )->name('api.video_view_site');

    Route::get(
        'views/{uploadvideo:slug}/{video:id}/videos',
        'VideoController@apivideosviewshow'
    )->name('api.videosvideo_view_site');

});

Route::get(
    'store/{videotype:slug}',
    'VideoController@videobyvideotype'
)->name('videobyvideotype_site');

Route::get(
    'details/{video:slug}',
    'VideoController@videoshow'
)->name('videoshow_site');

Route::get(
    'views/{uploadvideo:slug}/{video:id}',
    'VideoController@viewshow'
)->name('video_view_site');


Route::group(['middleware' => 'verified'], function(){


    require(__DIR__ . DIRECTORY_SEPARATOR . 'likes.php');
    require(__DIR__ . DIRECTORY_SEPARATOR . 'favorites.php');

    Route::group(['middleware' => 'verified_status_user'],function (){



    });

});

require(__DIR__ . DIRECTORY_SEPARATOR . 'acteur.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'boutique.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'search.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'comment'. DIRECTORY_SEPARATOR . 'index.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'pages.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'contact.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'videofilm.php');
require(__DIR__ . DIRECTORY_SEPARATOR . 'videoserie.php');
