<?php

use App\Http\Controllers\User\SearchController;

Route::get(
    'search',
    [SearchController::class, 'index']
)->name('search.index');
