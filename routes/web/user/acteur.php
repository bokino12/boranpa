<?php

Route::get(
    'api/actors/{user:slug}',
    'ActeurController@apiactors'
)->name('api.actors_site');

Route::get(
    'actors/{user:slug}',
    'ActeurController@index'
)->name('actors.index');

