<?php

Route::get(
    'contacts',
    'ContactController@create'
)->name('contacts.create');

Route::post(
    'contacts',
    'ContactController@store'
)->name('contacts.store');
