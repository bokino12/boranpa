<?php


use App\Http\Controllers\User\Comments\CommentAndResponsecommentController;

Route::group(['namespace' => 'Comments'], function(){

    require(__DIR__ . DIRECTORY_SEPARATOR . 'comments.php');

    Route::delete(
        'comments/{comment}/delete',
        [CommentAndResponsecommentController::class, 'destroy']
    )->name('comments.destroy');

    Route::put(
        'responsecomments/{responsecomment}/update',
        [CommentAndResponsecommentController::class, 'updateresponse']
    )->name('responsecomments.update');

    Route::post(
        'responsecomments/{responsecomment}/response',
        [CommentAndResponsecommentController::class, 'sendresponse']
    )->name('responsecomments.sendresponse');

    Route::delete(
        'responsecomments/{responsecomment}/delete',
        [CommentAndResponsecommentController::class, 'destroyresponse']
    )->name('responsecomments.destroy');
});
