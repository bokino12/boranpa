<?php


use App\Http\Controllers\User\Comments\CommentuploadvideoController;

Route::get(
    'api/views/{uploadvideo:slug}/{video:id}/comments',
    [CommentuploadvideoController::class, 'getcomment']
)->name('api.uploadvideo_comments');

Route::post(
    'views/{uploadvideo:slug}/{video:id}/comments',
    [CommentuploadvideoController::class, 'sendcomment']
)->name('uploadvideosendcomment_site');

Route::put(
    'views/{uploadvideo:slug}/{video:id}/comments/{comment:id}',
    [CommentuploadvideoController::class, 'updatecomment']
)->name('uploadvideoupdatecomment_site');

Route::post(
    'views/{uploadvideo:slug}/{video:id}/comments/{comment}/responses',
    [CommentuploadvideoController::class, 'sendresponsecomment']
)->name('uploadvideosendresponsecomment_site');
