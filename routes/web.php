<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** Route site */
require(__DIR__ . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'index.php');

/** Auth */
Route::mixin(new \Laravel\Ui\AuthRouteMethods());
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/new_todo', 'HomeController@new')->name('new_todo');
Route::post('/home/save', 'HomeController@store')->name('todos.store');


Route::get('api/todos', 'HomeController@api')->name('api.todos');


